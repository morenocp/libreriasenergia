float sensorsValue[8];
#define canalPresion 0
#define canalFuerzaV 1
#define canalFuerzaH 2
#define canalLVDTV 3
#define canalLVDTH 4
#define canalVolumen 5
#define canalContrapresion 6
#define canalConfinante 7



void sendJSONValues(){
  Serial.print("{\"S\":[");
  Serial.print(sensorsValue[canalPresion], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalFuerzaV], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalFuerzaH], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalLVDTV], 3);
  Serial.print(",");
  Serial.print(sensorsValue[canalLVDTH], 3);
  Serial.print(",");
  Serial.print(sensorsValue[canalVolumen], 2);
  Serial.print(",");
  Serial.print(sensorsValue[canalContrapresion], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalConfinante], 1);
  Serial.print("]}\n");
}

void setup() {
  Serial.begin(9600);

}

void loop() {
  sendJSONValues();
  delay(1000);
}
