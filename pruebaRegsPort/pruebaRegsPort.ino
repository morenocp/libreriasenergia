#include "bitman.h"
#include "ports2553.h"
#define ledVerde Port1->out.bit0
#define ledRojo Port1->out.bit6
#define push Port1->in.bit3

void print(char *msg,uint16_t addr){
  Serial.print(msg);
  Serial.print(addr,16);
  Serial.println("h");
  
}

void setup() {
  Serial.begin(115200);
  P1OUT=0;
  Serial.println("Registros P1");
  print("SizeOf(Byte)=",sizeof(unByte));
  print("SizeOf(Word)=",sizeof(unWord));
  print("SizeOf(MultiFormat)=",sizeof(unMultiFormat));
  print("IN=",(uint16_t )&Port1->in.all);
  print("out=",(uint16_t )&Port1->out.all);
  print("dir=",(uint16_t )&Port1->dir.all);
  print("ifg=",(uint16_t )&Port1->ifg.all);
  print("ies=",(uint16_t )&Port1->ies.all);
  print("ie=",(uint16_t )&Port1->ie.all);
   print("sel=",(uint16_t )&Port1->sel.all);
  print("SEL2=",(uint16_t )&PortSel2->p1Sel2);
  print("REN=",(uint16_t )&Port1->ren.all);
Port1->dir.bit0=1;
Port1->dir.bit6=1;


}

void loop() {
  ledVerde=push==0;
  ledRojo^=1;
  Serial.println(P1OUT);
  delay(500);  
}
