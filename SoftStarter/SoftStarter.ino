#include "bitman.h"
#include "SoftStarter.h"
#include "CFlag.h"
#include "PinOut.h"
#include "PinIn.h"
#include "PMCFirmataConstants.h"
#include "PMCFirmataParser.h"
#include "ports2553.h"
#include "timerA2553.h"
#include "Countdown.h"


#define PERIODE_PWM 16042
#define PORT_PWM P2OUT
#define pinPWM1 BIT1
#define pinPWM2 BIT6
#define PORT_SWITCH P1IN
#define SWITCH BIT3
#define PORT_LINE P1IN
#define LINE1 BIT5
#define LINE2 BIT4
#define PORT_RELAY P2OUT
#define RELAY1 BIT5
#define RELAY2 BIT4
#define MAX_RAMPTIME 15.0
#define MIN_RAMPTIME 0.1
#define MAX_TPWM PERIODE_PWM
#define MIN_TPWM 10

unsigned char firmatabuffer[32];
FirmataParser firmata(firmatabuffer,sizeof(firmatabuffer));

SoftStarter line1SF(PERIODE_PWM+3);
SoftStarter line2SF(PERIODE_PWM+3);
CFlag enviar,startUp,startDown;
PinOut relay1(PORT_RELAY, RELAY1);
PinOut relay2(PORT_RELAY, RELAY2);
PinOut ledIsRun(P1OUT, BIT6);
PinIn boton(PORT_SWITCH,SWITCH);
PinIn line1(PORT_LINE,LINE1);
PinIn line2(PORT_LINE,LINE2);
Countdown sendInfo(64000);
float rampTime=MIN_RAMPTIME;
uint16_t tPWM=PERIODE_PWM;



void clearPWM() {
  
  BIS(TA1CTL, TACLR);
  BIS(TA1CTL, TASSEL_2 | ID_3);
  BIS(TA1CTL, MC_1);
}

void setPWM(uint16_t value) {
  TA1CCR1 = value;
}

void enablePWM(bool enable,uint8_t pwmPin) {
  if (enable) BIS(P2SEL, pwmPin);
  else BIC(P2SEL, pwmPin);
}

void configPWM() {
  BIC(P2OUT, pinPWM1);
  BIS(P2DIR, pinPWM1);
  TA1CCR0 = PERIODE_PWM; //Frecuencia PWM=32768/256=128Hz
  BIS(TA1CCTL1, OUTMOD_3); //TimerA1 Reg1 en modo PWM ResetSet
  BIS(TA1CTL, TASSEL_2 | ID_3 | MC_1);
}


void enableIntLine(bool enable,uint8_t line) {
  if (enable) BIS(P1IE, LINE1); else BIC(P1IE, LINE1);
}


void setEdgeIntSw(bool isFalling) {
  switch (isFalling) {
    case false:           //Rising
      BIC(P1IES, SWITCH);
      break;
    case true:         //Falling
      BIS(P1IES, SWITCH);
      break;
  }
}


void ISR_CERO() {
  uint8_t reg = P1IFG;
  
  if (BTS(reg, SWITCH)) {  
      //start.set();
      
  }
  if (BTS(reg, LINE1)) {
    if (line1SF.next()) {
      clearPWM();
      setPWM(line1SF.getValue());
      enviar.set();
    } else {
      enableIntLine(false,LINE1);
     // enablePWM(false,pinPWM1);
    }
    relay1.set(line1SF.getRelayState());
  }

  BIC(P1IFG, reg);
}

void adcReadParameters(){
  rampTime = MAX_RAMPTIME*analogRead(P1_6)/1023+MIN_RAMPTIME;
  tPWM = MAX_TPWM*analogRead(P1_7)/1023+MIN_TPWM;
}

void setBoolData(void * context, uint8_t channel, uint16_t value){
  if(channel==3){
      if(value==1) {
        startUp.set();
        //Serial.println("Arrancando...");
      } else
      {
        startDown.set();
       // debugPrint("Detenido");
      }
  }
}

void setInt14Data(void * context, uint8_t channel, uint16_t value){
  switch(channel){
    case 2:
      tPWM=value;
      //Serial.print("tPWM=");
      //Serial.println(value);
      break;
    case 1:
    rampTime=(float)value/1000.0;
    //debugPrint("rampTime=");
    //debugPrintln(rampTime,2);
     break;   
  }
}


void setupCallback(){
  firmata.attach(ANALOG_MESSAGE,setInt14Data,0);
  firmata.attach(SET_DIGITAL_PIN_VALUE,setBoolData,0);
  
}

void setup() {
  P2OUT=0;

  configPWM();
  Serial.begin(115200);
  Serial.println("Setup system...");
  setupCallback();
  enablePWM(false,pinPWM1);
  enablePWM(false,pinPWM2);
  
  setPWM(line1SF.getValue());
  clearPWM();


  // Habilita interrupcion por flanco de bajada de P1.5
  /*PxIFG 1=interrupt pendiente
    PxIES 0=low2high, 1=high2low
    PxIE  1=enable interrupt*/
  BIS(P1IES, LINE1 | SWITCH); //Ambas interrupciones seran en flanco de bajada
  BIC(P1IFG, LINE1 | SWITCH); //limpia banderas de interrupcion
  BIS(P1IE, SWITCH);      //Habilita interrupcion del switch
 // attachInterrupt(P1_7, ISR_CERO, FALLING);
 Serial.println("System ready");
}

void loop() {
  

  ledIsRun.set(BTS(P1IE, LINE1));
  if(Serial.available()) firmata.parse(Serial.read());
  
  if(startUp.isSet()){
    
    line1SF.setPeriodePWM(tPWM);
    line1SF.setRampTime(rampTime);
    delay(300);
    enablePWM(true,pinPWM1);
    bool isButtonPress=true; //boton.isClear(); 
    line1SF.start(isButtonPress); 
    setEdgeIntSw(!isButtonPress); 
    enableIntLine(true,LINE1);
    
  }
  if(startDown.isSet()){
    enablePWM(false,pinPWM1);
    line1SF.start(false); 
    enableIntLine(true,LINE1);
  }
  
  if (sendInfo.isZero()){
    Serial.print("rt[s]= ");
    Serial.print(rampTime,3);
    Serial.print(" angle=");   
    Serial.println(tPWM*180.0/PERIODE_PWM,0);
   
  }
}



  #pragma vector= PORT1_VECTOR //vector de interrupcion del puerto1
  __interrupt void isr_port1(void) { //rutina de servicio
  ISR_CERO();
  }
