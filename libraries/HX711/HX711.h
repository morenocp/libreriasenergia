/*
 * HX711.h
 *
 *  Created on: 25 abr. 2017
 *      Author: pedro
 */

#ifndef HX711_H_
#define HX711_H_

#include "PinOut.h"
#include "PinIn.h"

class HX711
{
    private:
        PinOut* PD_SCK;    // Power Down and Serial Clock Input Pin
        PinIn* DOUT;      // Serial Data Output Pin
        unsigned char GAIN;      // amplification factor
        long OFFSET;    // used for tare weight
        float SCALE;    // used to return weight in grams, kg, ounces, whatever
        long lastvalue,average;

    public:
        // define clock and data pin, channel, and gain factor
        // channel selection is made by passing the appropriate gain: 128 or 64 for channel A, 32 for channel B
        // gain: 128 or 64 for channel A; channel B works with 32 gain factor only
        HX711(PinIn& dout, PinOut& pd_sck);

        virtual ~HX711();

        // Allows to set the pins and gain later than in the constructor
        void begin(unsigned char gain = 128);

        // check if HX711 is ready
        // from the datasheet: When output data is not ready for retrieval, digital output pin DOUT is high. Serial clock
        // input PD_SCK should be low. When DOUT goes to low, it indicates data is ready for retrieval.
        bool isReady();

        // set the gain factor; takes effect only after a call to read()
        // channel A can be set for a 128 or 64 gain; channel B has a fixed 32 gain
        // depending on the parameter, the channel is also set to either A or B
        void setGain(unsigned char gain = 128);

        // waits for the chip to be ready and returns a reading
        long read();

        // returns an average reading; times = how many times to read
        long getAverage();

        // returns (lastValue- OFFSET), that is the current value without the tare weight; times = how many readings to do
        long getValue();

       // returns (lastValue)
        long getRawValue();

        // returns get_value() divided by SCALE, that is the raw value divided by a value obtained via calibration
        // times = how many readings to do
        float getUnits();

        // returns (value-Offset)  divided by SCALE, that is the raw value divided by a value obtained via calibration

        float getUnits(long value);

        // set the OFFSET = average
        long tare();

        // set the SCALE value; this value is used to convert the raw data to "human readable" data (measure units)
        void setScale(float scale = 1.f);

        // get the current SCALE
        float getScale();

        // set OFFSET, the value that's subtracted from the actual reading (tare weight)
        void setOffset(long offset = 0);

        // get the current OFFSET
        long getOffset();

        // puts the chip into power down mode
        void powerDown();

        // wakes up the chip after power down mode
        void powerUp();
};

#endif /* HX711_H_ */
