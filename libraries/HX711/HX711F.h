/*
 * HX711F.h
 *
 *  Created on: 25 abr. 2017
 *      Author: pedro
 */

#ifndef HX711F_H_
#define HX711F_H_



class HX711F
{
    private:
        long OFFSET;    // used for tare weight
        float SCALE;    // used to return weight in grams, kg, ounces, whatever
        long lastvalue,average;
   protected:
	void updateAverage();
	

    public:
        // define clock and data pin, channel, and gain factor
        // channel selection is made by passing the appropriate gain: 128 or 64 for channel A, 32 for channel B
        // gain: 128 or 64 for channel A; channel B works with 32 gain factor only
        HX711F(float scale=1.0,long offset=0);

        virtual ~HX711F(){};

	void setFromFirmata(unsigned char argc, unsigned char * argv);


       long read();

        // returns an average reading; times = how many times to read
        long getAverage();

        // returns (lastValue- OFFSET), that is the current value without the tare weight; times = how many readings to do
        long getValue();

        // returns get_value() divided by SCALE, that is the raw value divided by a value obtained via calibration
        // times = how many readings to do
        float getUnits();

        // returns (value-Offset)  divided by SCALE, that is the raw value divided by a value obtained via calibration

        float getUnits(long value);

        // set the OFFSET = average
        long tare();

        // set the SCALE value; this value is used to convert the raw data to "human readable" data (measure units)
        void setScale(float scale = 1.f);

        // get the current SCALE
        float getScale();

        // set OFFSET, the value that's subtracted from the actual reading (tare weight)
        void setOffset(long offset = 0);

        // get the current OFFSET
        long getOffset();
	
	// set LastValue variable from external value and update average variable
	void setValueFromLong(long value);

       
};

#endif /* HX711F_H_ */
