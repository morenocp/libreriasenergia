/*
 * HX711F.cpp
 *
 *  Created on: 25 abr. 2017
 *      Author: pedro
 */

#include <HX711F.h>

HX711F::HX711F(float scale,long offset) {
    OFFSET=0;
    SCALE=1.0f;
    lastvalue=0;
    average=0;
}



void HX711F::updateAverage(){
	average=lastvalue+average-((average-8)>>4);
}
void HX711F::setValueFromLong(long value){
	lastvalue=value;
	updateAverage();
}


void HX711F::setFromFirmata(unsigned char argc, unsigned char * argv) {
   int n=argc-1;
   unsigned char i;
   unsigned long value=argv[n];
   for(i=n;i>1;i--) value=(value<<7)|argv[i-1];
   setValueFromLong(static_cast<long>(value));
  // lastvalue=static_cast<long>(value);
  // average=lastvalue+average-((average-8)>>4);
 }

long HX711F::read(){
return lastvalue;
}

long HX711F::getAverage() {
    return average>>4;
}

long HX711F::getValue() {
    return lastvalue - OFFSET;
}



float HX711F::getUnits() {
    return getUnits(lastvalue);
}

float HX711F::getUnits(long value) {
    return (value-OFFSET) / SCALE;
}

long HX711F::tare() {
    long sum = getAverage();
    setOffset(sum);
    return sum;
}

void HX711F::setScale(float scale) {
    SCALE = scale;
}

float HX711F::getScale() {
    return SCALE;
}

void HX711F::setOffset(long offset) {
    OFFSET = offset;
}

long HX711F::getOffset() {
    return OFFSET;
}



