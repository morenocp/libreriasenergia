/*
 * HX711.cpp
 *
 *  Created on: 25 abr. 2017
 *      Author: pedro
 */

#include <HX711.h>

HX711::HX711(PinIn& dout, PinOut& pd_sck) {
    DOUT=&dout;
    PD_SCK=&pd_sck;
    OFFSET=0;
    SCALE=1.0f;
    lastvalue=0;
    average=0;
}


HX711::~HX711() {
}

void HX711::begin(unsigned char gain) {
    setGain(gain);
}

bool HX711::isReady() {
    return DOUT->isClear(); // digitalRead(DOUT) == LOW;
}

void HX711::setGain(unsigned char gain) {

    switch (gain) {
        case 128:       // channel A, gain factor 128
            GAIN = 1;
            break;
        case 64:        // channel A, gain factor 64
            GAIN = 3;
            break;
        case 32:        // channel B, gain factor 32
            GAIN = 2;
            break;
    }

   // PD_SCK->clear(); //digitalWrite(PD_SCK, LOW);
    //read();
}

long HX711::read() {
unsigned char i;
unsigned long value = 0;
    // wait for the chip to become ready
    while (!isReady());

    for (i = 0; i < 24; i++) {
            PD_SCK->set(); //digitalWrite(PD_SCK, HIGH);
            value=value<<1;
            PD_SCK->clear();//digitalWrite(PD_SCK, LOW);
            if(DOUT->isSet()) value++;
        }

    // set the channel and the gain factor for the next reading using the clock pin
    for ( i = 0; i < GAIN; i++) {
        PD_SCK->set(); //digitalWrite(PD_SCK, HIGH);
        PD_SCK->clear();//digitalWrite(PD_SCK, LOW);
    }

    if(value&0x800000) value=value|0xff000000;
    lastvalue=static_cast<long>(value);
    average=lastvalue+average-((average-8)>>4);
    return lastvalue;
}


long HX711::getRawValue(){
return lastvalue;
}

long HX711::getAverage() {
    return average>>4;
}

long HX711::getValue() {
    return lastvalue - OFFSET;
}



float HX711::getUnits() {
    return getUnits(lastvalue);
}

float HX711::getUnits(long value) {
    return (value-OFFSET) / SCALE;
}

long HX711::tare() {
    long sum = getAverage();
    setOffset(sum);
    return sum;
}

void HX711::setScale(float scale) {
    SCALE = scale;
}

float HX711::getScale() {
    return SCALE;
}

void HX711::setOffset(long offset) {
    OFFSET = offset;
}

long HX711::getOffset() {
    return OFFSET;
}

void HX711::powerDown() {
    PD_SCK->clear();//digitalWrite(PD_SCK, LOW);
    PD_SCK->set(); //digitalWrite(PD_SCK, HIGH);
}

void HX711::powerUp() {
    PD_SCK->clear(); //digitalWrite(PD_SCK, LOW);
}


