/*
 * CharBuffer.h
 *
 *  Created on: 20/06/2014
 *      Author: pedro
 */

#ifndef BYTEBUFFER_H_
#define BYTEBUFFER_H_

class ByteBuffer {
public:
	//ByteBuffer(unsigned char size=16);
	ByteBuffer(unsigned char *buffer,unsigned char size);
	~ByteBuffer(){};
	unsigned char isEmpty();
	unsigned char isFull();
	unsigned char available();
	void push(char data);
	char pop();
	char read(){return pop();};
	void clear();
private:
	unsigned char size,head,tail;
	char* buffer;
};

#endif /* BYTEBUFFER_H_ */
