/*
 * LinearMap.cpp
 *
 *  Created on: 24 jul. 2019
 *      Author: pedro
 */

#include <LinearMap.h>

LinearMap::LinearMap()
{
   slope=1;
   offset=0;
}

LinearMap::LinearMap(float varSlope,float varOffset)
{
  slope=1;
  offset=0;
  update(varSlope,varOffset);
}

float LinearMap::getSlope(){
    return slope;
}

float LinearMap::getOffset(){
        return offset;
    }


bool LinearMap::update(float varSlope,float varOffset){

         if(setSlope(varSlope)) {
             setOffset(varOffset);
             return true;
         } else return false;
    }


bool LinearMap::update (float x1, float y1, float x2,float y2){
        float dx=x2-x1;
        if(dx!=0 && setSlope((y2-y1)/dx)) {
            setOffset(y1-slope*x1);
            return true;
        } else return false;
    }


float LinearMap::map(float x){
        return slope*x+offset;
    }


float LinearMap::imap (float y){
        return (y-offset)/slope;
    }

float LinearMap::delta(float x){
        return slope*x;
    }


float LinearMap::idelta (float y){
        return y/slope;
    }

bool  LinearMap::setSlope(float varSlope){
    bool result=varSlope!=0;
    if(result) slope=varSlope;
    return result;
}

void LinearMap::setOffset(float varOffset){
    offset=varOffset;
}



