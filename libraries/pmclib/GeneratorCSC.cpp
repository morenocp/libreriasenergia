/*
 * GeneratorCSC.cpp
 *
 *  Created on: 25 jul. 2019
 *      Author: pedro
 */

#include <GeneratorCSC.h>

GeneratorCSC::GeneratorCSC(const float *data, unsigned int ndata)
:FunctionGenerator(data,ndata)
{
    enable=false;
    stop=false;
    amplitud=1.0;
    offset=0;
    phaseIntervalValue=true;
    phaseInterval1=0;
    phaseInterval2=ndata-1;
}

void GeneratorCSC::setIntervalValues(unsigned int phase1, unsigned int phase2,bool value){
    phaseInterval1=phase1;
    phaseInterval2=phase2;
    phaseIntervalValue=value;
}

void GeneratorCSC::setAmplitud(float value){
    amplitud=value;
}

void GeneratorCSC::setOffset(float value){
    offset=value;
}

float GeneratorCSC::getAmplitud(){
    return amplitud;
}

float GeneratorCSC::getOffset(){
    return offset;
}

void GeneratorCSC::setEnable(){
    enable=true;
    stop=false;
    cycleNumber=0;
    reset();
}
bool GeneratorCSC::getEnable(){
    return enable;
}
void GeneratorCSC::setStop(){
    stop=true;;
}
float GeneratorCSC::getValue(bool plusOffset){
    actualValue=amplitud*getData();
    if(plusOffset) actualValue+=offset;
    return actualValue;
}
bool GeneratorCSC::next(){
    bool result=false;
    if(enable){
        result=getPhase()==0;
        if(result) cycleNumber++;
        if(result &&  stop){
            enable=false;
            stop=false;
        } else result=false;
        prevValue=actualValue;
        nextPhase();
    }
    return result;
}


bool GeneratorCSC::isSlopeUp(){
    unsigned int phase=getPhase();
    if(phase>=phaseInterval1 && phase<=phaseInterval2) return phaseIntervalValue;
    else return !phaseIntervalValue;
}

unsigned int GeneratorCSC::getCycleNumber(){
    return cycleNumber;
}

float GeneratorCSC::getPrevValue(){
    return prevValue;
}

void GeneratorCSC::setPrevValue(float value){
    prevValue=value;
}

float GeneratorCSC::getDeltaValue(bool plusOffset){
    return getValue(plusOffset)-prevValue;
}
