/*
 * lpg2.h
 *
 *  Created on: 05/12/2014
 *      Author: pedro
 */

#ifndef LPG2_H_
#define LPG2_H_
#include "bitman.h"

//Definiciones led rojo
#define ledRed BIT0
#define ledRedSetup() 	BIS(P1DIR,ledRed)
#define ledRedOn() 		BIS(P1OUT,ledRed)
#define ledRedOff() 	BIC(P1OUT,ledRed)
#define ledRedTgl() 	BIX(P1OUT,ledRed)

//Definiciones led verde
#define ledGreen BIT6
#define ledGreenSetup() BIS(P1DIR,ledGreen)
#define ledGreenOn() 	BIS(P1OUT,ledGreen)
#define ledGreenOff() 	BIC(P1OUT,ledGreen)
#define ledGreenTgl() 	BIX(P1OUT,ledGreen)

//Definiciones ambos led
#define ledsSetup()  	BIS(P1DIR,ledRed+ledGreen)
#define ledsOn() 		BIS(P1OUT,ledRed+ledGreen)
#define ledsOff() 		BIC(P1OUT,ledRed+ledGreen)
#define ledsTgl() 		BIX(P1OUT,ledRed+ledGreen)

//Definiciones para el boton
#define button BIT3
#define buttonIsUp() 	(P1IN & button)
#define buttonIsDown() 	!buttonIsUp()
#define buttonSetup() 	st( BIC(P1DIR,button);BIS(P1REN,button);BIS(P1OUT,button);)
#define buttonIE_ON() 	st( BIS(P1IE,button); BIS(P1IES,button);)
#define buttonRETI() 	BIC(P1IFG,button)



/* Use el vector del puerto para manejar la interrupcion, ejemplo:
 *
 #pragma vector= PORT1_VECTOR
__interrupt void isr_puerto1(void){
	Inserte su código aqui
	buttonRETI(); //limpia la bandera de interrupcion
}
 */

//Definiciones para calibrar el DCO utilizando constantes predefinidas por TI

#define clockSetup1MHz() 	st(BCSCTL1 = CALBC1_1MHZ;DCOCTL = CALDCO_1MHZ;)
#define clockSetup8MHz() 	st(BCSCTL1 = CALBC1_8MHZ;DCOCTL = CALDCO_8MHZ;)
#define clockSetup12MHz() 	st(BCSCTL1 = CALBC1_12MHZ;DCOCTL = CALDCO_12MHZ;)
#define clockSetup16MHz() 	st(BCSCTL1 = CALBC1_16MHZ;DCOCTL = CALDCO_16MHZ;)



//Definiciones varias
#define WDTI_ON() 		BIS(IE1,WDTIE)
#define GIE_ON() 		_BIS_SR(GIE)
#define NMIE_ON() 		BIS(IE1,NMIIE)
#define NMI_RETI() 		st(BIC(IFG1,NMIIFG);NMIE_ON;)
#define SMCLK_SHOW() 	st( BIS(P1DIR,BIT4); BIS(P1SEL,BIT4);)  			//selecciona la funcion periferica SMCLK
#define XT_OFF() 		st(BIC(P2SEL,BIT6+BIT7);BIC(P2DIR,BIT6+BIT7);)		//Selecciona funcion digital de P2.6-7
#define WDT_1SEG() 		WDTPW|WDTTMSEL|WDTSSEL							//WD modo intervalos de tiempo, fuente=ACLK/32768
#define WDT_1Hz() 		WDTPW|WDTTMSEL|WDTSSEL							//WD modo intervalos de tiempo, fuente=ACLK/32768
#define WDT_4Hz() 		WDTPW|WDTTMSEL|WDTSSEL|WDTIS0						//WD modo intervalos de tiempo, fuente=ACLK/8198
#define WDT_64Hz() 		WDTPW|WDTTMSEL|WDTSSEL|WDTIS1					//WD modo intervalos de tiempo, fuente=ACLK/512
#define WDT_512Hz() 	WDTPW|WDTTMSEL|WDTSSEL|WDTIS0|WDTIS1			//WD modo intervalos de tiempo, fuente=ACLK/64

/*
#pragma vector= WDT_VECTOR //vector de interrupcion del WDT
__interrupt void isr_wdt(void){ //rutina de servicio
}
*/



#endif /* LPG2_H_ */
