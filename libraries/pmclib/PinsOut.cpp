#include "PinsOut.h"
#include "bitman.h"

volatile unsigned char* PinsOut::port;

PinsOut::PinsOut(volatile unsigned char &port,volatile unsigned char &portdir, unsigned char pin, unsigned char initvalue){
  if(PinsOut::port==0) PinsOut::port=&port;
  this->pin=pin;
  set(initvalue);
  BIS(portdir,pin);
}

PinsOut::PinsOut(volatile unsigned char &port, unsigned char pin, unsigned char initvalue){
  if(PinsOut::port==0) PinsOut::port=&port;
  this->pin=pin;
  set(initvalue);
  BIS(*(PinsOut::port+1),pin);
}

  void PinsOut::set(){
   BIS(*port,pin);
  }
  void PinsOut::clear(){
    BIC(*port,pin);
  }
  void PinsOut::set(bool value){
    if(value) set(); else clear();
  }
void PinsOut::toggle(){
    BIX(*port,pin);
  }

