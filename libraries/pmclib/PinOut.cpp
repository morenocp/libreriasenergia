#include "PinOut.h"
#include "bitman.h"

PinOut::PinOut(volatile unsigned char &port, unsigned char pin, volatile unsigned char &portdir,unsigned char initvalue){
  this->port=&port;
  this->pin=pin;
  set(initvalue);
  BIS(portdir,pin);
  
}

PinOut::PinOut(volatile unsigned char &port, unsigned char pin,unsigned char initvalue){
  this->port=&port;
  this->pin=pin;
  set(initvalue);
  BIS(*(this->port+1),pin); //BIS(PxDIR,pin)
  
}




  void PinOut::set(){
    BIS(*port,pin);
  }
  void PinOut::clear(){
    BIC(*port,pin);
  }
  void PinOut::set(bool value){
    if(value) set(); else clear();
  }
void PinOut::toggle(){
    BIX(*port,pin);
  }

