/*
 * SoftStarter.h
 *
 *  Created on: 25/02/2020
 *      Author: pedro
 */

#ifndef SOFTSTARTER_H_
#define SOFTSTARTER_H_

//typedef unsigned int uint16_t;
#include "stdint.h"

class SoftStarter
{
private:
    uint16_t  tPWM,f2,incPWM,value,limit;
    bool up;
    void initValue();

public:
    SoftStarter(uint16_t  tPWM,uint16_t frecuency=60);
    void setRampTime(float rampTime);
    bool getRelayState();
    void start(bool isRising);
    bool next();
    bool isRun();
    bool isRising();
    uint16_t getValue();
    void setPeriodePWM(uint16_t valuePWM);
};

#endif /* SOFTSTARTER_H_ */
