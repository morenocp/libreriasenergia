/*
 * CFlag.h
 *
 *  Created on: 15 feb. 2019
 *      Author: pedro
 */

#ifndef CFLAG_H_
#define CFLAG_H_

class CFlag
{
private:
    bool flag;
public:
    CFlag(bool iniState=false);
    bool isSet();
    void set ();

};

#endif /* CFLAG_H_ */
