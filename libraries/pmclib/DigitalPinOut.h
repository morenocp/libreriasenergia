/*
 * DigitalPinOut.h
 *
 *  Created on: 21 abr. 2020
 *      Author: pedro
 */

#ifndef DIGITALPINOUT_H_
#define DIGITALPINOUT_H_

#include <DigitalPin.h>

class DigitalPinOut: public DigitalPin
{
private :
    void config();
public:
    DigitalPinOut(ptRegsPort port, uint8_t bit, bool isSet=false);
    void set();
    void set(bool val);
    void clear();
    void toogle();
};

#endif /* DIGITALPINOUT_H_ */
