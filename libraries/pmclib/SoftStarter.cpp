/*
 * SoftStarter.cpp
 *
 *  Created on: 25/02/2020
 *      Author: pedro
 */

#include <SoftStarter.h>

SoftStarter::SoftStarter(uint16_t periodePWM,uint16_t frecuency)
{
    tPWM=periodePWM;
    f2=2*frecuency;
    up=true;
    initValue();
}


void SoftStarter::setRampTime(float rampTime) {
    incPWM=(uint16_t) (tPWM/(f2*rampTime));
}

void SoftStarter::start(bool isRising) {
    up=isRising;
    initValue();
}

bool SoftStarter::getRelayState() {
    return up && (value==0);
}

bool SoftStarter::isRising() {
    return up;
}

bool SoftStarter::isRun() {
    if(up) return value>0;
    else return value<tPWM;
}

uint16_t SoftStarter::getValue() {
    return value;
}

bool SoftStarter::next(){
    bool result;
    if(up) {
        result=value!=0;
        value=value>=limit?value-incPWM:0;
    } else {
        result=value!=tPWM;
        value=value<=limit?value+incPWM:tPWM;
    }
    return result;
}

void SoftStarter::initValue() {
    value=up?tPWM:0;
    limit=up?incPWM:tPWM-incPWM;
}

void SoftStarter::setPeriodePWM(uint16_t valuePWM){
tPWM=valuePWM;
}
