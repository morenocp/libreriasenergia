/*
 * CFlag.cpp
 *
 *  Created on: 15 feb. 2019
 *      Author: pedro
 */

#include <CFlag.h>

CFlag::CFlag(bool iniState)
{
    flag=iniState;

}
bool CFlag::isSet(){
    bool result=flag;
    flag=false;
    return result;
}

void CFlag::set(){
    flag=true;
}

