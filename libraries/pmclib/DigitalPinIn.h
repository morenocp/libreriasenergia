/*
 * DigitalPinIn.h
 *
 *  Created on: 21 abr. 2020
 *      Author: pedro
 */

#ifndef DIGITALPININ_H_
#define DIGITALPININ_H_

#include <DigitalPin.h>

class DigitalPinIn: public DigitalPin
{

public:
    enum INPUT_MODE {
        INPUT_NO_RESISTOR,
        INPUT_RES_PULLUP,
        INPUT_RES_PULLDOWN,
    } ;
    DigitalPinIn(ptRegsPort port, uint8_t bit,DigitalPinIn::INPUT_MODE  mode=INPUT_RES_PULLUP);
    bool isSet();
    bool isClear();
    bool isEnableInt();
    bool isIntFlag();
    void enableInt(bool enable=true);
    void clearIntFlag();
    void setIntEdge(bool hi2low=true);
protected :
    void config(DigitalPinIn::INPUT_MODE mode);
};

#endif /* DIGITALPININ_H_ */
