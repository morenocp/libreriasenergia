/*
 * ports2553.h
 *
 *  Created on: 14 abr. 2020
 *      Author: pedro
 */

#ifndef PORTS2553_H_
#define PORTS2553_H_

#include "stdint.h"

#ifdef __cplusplus
extern "C"
{
#endif

//#include "unionsByte.h"
//
//typedef struct {
//    unByte in;
//    unByte out;
//    unByte dir;
//    unByte ifg;
//    unByte ies;
//    unByte ie;
//    unByte sel;
//    unByte ren;
//} stRegsPort;
//
//#pragma pack(1)
//typedef union {
//    unByte pxSel2[2];
//    struct{
//        unByte p1Sel2;
//        unByte p2Sel2;
//    };
//} unSel2Port;

#pragma pack(push, 1)
typedef struct {
    volatile uint8_t in;
    volatile uint8_t out;
    volatile uint8_t dir;
    volatile uint8_t ifg;
    volatile uint8_t ies;
    volatile uint8_t ie;
    volatile uint8_t sel;
    volatile uint8_t ren;
} stRegsPort;


typedef union {
    volatile uint8_t pxSel2[2];
    struct{
        volatile uint8_t p1Sel2;
        volatile uint8_t p2Sel2;
    };
} unSel2Port;
#pragma pack(pop)

typedef stRegsPort* volatile ptRegsPort;
typedef unSel2Port* volatile ptSel2Port;
;

extern const ptRegsPort Port1;
extern const ptRegsPort Port2;
extern const ptSel2Port PortSel2;


#ifdef __cplusplus
}
#endif

#endif /* PORTS2553_H_ */
