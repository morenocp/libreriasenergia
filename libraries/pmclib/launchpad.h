/*
 * launchpad.h
 *
 *  Created on: 08/06/2015
 *      Author: pedro
 */

#ifndef LAUNCHPAD_H_
#define LAUNCHPAD_H_

#if defined (__MSP430F2274__)
#include "lpf2274.h"

#elif defined (__MSP430F5529__)
#include "lpf5529.h"

#elif defined (__MSP430G2553__) || defined (__MSP430F2013__) || defined (__MSP430G2452__)
#include "lpg2.h"

#elif defined (__MSP430G2231__) || defined (__MSP430F2211__)
#include "lpg2.h"


#endif

#endif /* LAUNCHPAD_H_ */
