/*
 * bitman.h
 *
 *  Created on: 10 ene. 2017
 *      Author: pedro
 */

#ifndef BITMAN_H_
#define BITMAN_H_



/* ------------------------------------------------------------------------------------------------
 *                                          Macros
 * ------------------------------------------------------------------------------------------------
 */
/* bit value */
#ifndef BV
#define BV(n)      (1 << (n))
#endif

#ifndef st
#define st(x)      do { x } while (__LINE__ == -1)
#endif

#define BIS(BISPort,BISPort_bit) BISPort|=(BISPort_bit)
#define BIC(BISPort,BISPort_bit) BISPort&=~(BISPort_bit)
#define BIX(BISPort,BISPort_bit) BISPort^=(BISPort_bit)
#define BTS(BISPort,BISPort_bit) BISPort&(BISPort_bit)
#define XOR(BISPort,BISPort_bit) BISPort^=(BISPort_bit)

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define constrain(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))
#define constrain2(amt,low,high) ((amt)<(low)?(high):((amt)>(high)?(low):(amt)))



#endif /* BITMAN_H_ */
