/*
 * PinIn.cpp
 *
 *  Created on: 24 abr. 2017
 *      Author: pedro
 */

#include <PinIn.h>
#include "bitman.h"

PinIn::PinIn(const volatile unsigned char& port, unsigned char pin)
{
    this->port=(volatile unsigned char*) (&port);
    this->pin=pin;
    BIC(*(this->port+2),pin); // BIC(PXDIR,pin)
    BIS(*(this->port+7),pin); // BIS(PXREN,pin)		
    BIS(*(this->port+1),pin); // BIS(PXOUT,pin)
}

PinIn::PinIn(const volatile unsigned char& port,unsigned char pin,
            volatile unsigned char& portdir,
            volatile unsigned char& portren,
            volatile unsigned char& portout,
            unsigned char pulltype){
    this->port=(volatile unsigned char*) (&port);
    this->pin=pin;
    BIC(portdir,pin);
    switch(pulltype){
	case 0:
		BIC(portren,pin);
		break;
	case 1:
		BIS(portren,pin);
		BIS(portout,pin);
		break;
	case 2:
		BIS(portren,pin);
		BIC(portout,pin);
		break;
	}
    }



bool PinIn::isSet(){
    return BTS(*port,pin);
}
 
bool PinIn::isClear(){
     return !isSet();
 }
