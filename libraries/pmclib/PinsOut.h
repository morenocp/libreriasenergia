#include "IPinOut.h"
class PinsOut:public IPinOut{
  static volatile unsigned char *port;
  public:
  PinsOut(volatile unsigned char &port,volatile unsigned char &portdir, unsigned char pin, unsigned char initvalue=0);
  PinsOut(volatile unsigned char &port,unsigned char pin, unsigned char initvalue=0);
  void set();
  void clear();
  void set(bool value);
  void toggle(); 
};

