/*
 * FunctionGenerator.h
 *
 *  Created on: 25 jul. 2019
 *      Author: pedro
 */

#ifndef FUNCTIONGENERATOR_H_
#define FUNCTIONGENERATOR_H_

class FunctionGenerator
{
    const float *tableData;
    unsigned int nData,phase;
public:
    FunctionGenerator(const float *data, unsigned int ndata);
    void nextPhase();
    unsigned int getPhase();
    float getData();
    void reset();
};

#endif /* FUNCTIONGENERATOR_H_ */
