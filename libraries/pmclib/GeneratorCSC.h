/*
 * GeneratorCSC.h
 *
 *  Created on: 25 jul. 2019
 *      Author: pedro
 */

#ifndef GENERATORCSC_H_
#define GENERATORCSC_H_

#include "FunctionGenerator.h"

class GeneratorCSC: public FunctionGenerator
{
private:
    bool enable,stop,phaseIntervalValue;
    float amplitud, offset,prevValue,actualValue;
    unsigned int phaseInterval1,phaseInterval2,cycleNumber;

public:
    GeneratorCSC(const float *data, unsigned int ndata);
    void setIntervalValues(unsigned int phase1, unsigned int phase2,bool value);
    void setAmplitud(float value);
    void setOffset(float value);
    float getAmplitud();
    float getOffset();
    void setEnable();
    bool getEnable();
    void setStop();
    float getValue(bool plusOffset=true);
    bool next();
    bool isSlopeUp();
    unsigned int getCycleNumber();
    float getDeltaValue(bool plusOffset=true);
    float getPrevValue();
    void setPrevValue(float value);
};

#endif /* GENERATORCSC_H_ */
