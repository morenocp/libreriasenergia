/*
 * NuevoUSCI.h
 *
 *  Created on: 03/02/2015
 *      Author: pedro
 */

#ifndef NUEVOUSCI_H_
#define NUEVOUSCI_H_
//#define NothingFunction(var,function) var=var==0?&function:var
#define i2cSDA BIT7
#define i2cSCL BIT6
#define uartTX BIT2
#define uartRX BIT1

#include <bitman.h>
#include <msp430.h>
#include <ByteBuffer.h>


/********************************************************************************************************************/
class USCIVector {
private:
	static unsigned char (*ucaRxFunction)(unsigned char);
	static unsigned char (*ucaTxFunction)(volatile unsigned char*);
	static unsigned char (*ucbRxFunction)(unsigned char );
	static unsigned char (*ucbTxFunction)(volatile unsigned char*);
	static unsigned char (*i2cSCFunction)(volatile unsigned char*);
	static unsigned char i2cMode;
	static __interrupt void USCIAB0TX_ISR(void);
	static __interrupt void USCIAB0RX_ISR(void);

public:
	USCIVector(){};
	~USCIVector(){};
	static void setUCARXFunction(unsigned char (*rxFunction)(unsigned char)){
		USCIVector::ucaRxFunction=rxFunction;
	}

	static void setUCBRXFunction(unsigned char (*rxFunction)(unsigned char)){
			USCIVector::ucbRxFunction=rxFunction;
		}

	static void setUCATXFunction(unsigned char (*txFunction)(volatile unsigned char*)){
					USCIVector::ucaTxFunction=txFunction;
				}

	static void setUCBTXFunction(unsigned char (*txFunction)(volatile unsigned char*)){
					USCIVector::ucbTxFunction=txFunction;
				}

	static void setI2CSCFunction(unsigned char (*scFunction)(volatile unsigned char*)){
					USCIVector::i2cSCFunction=scFunction;
				}

	static void setI2CMode(unsigned char mode){USCIVector::i2cMode=mode;}
	static unsigned char getI2CMode(void){return USCIVector::i2cMode;}

};

/********************************************************************************************************************/
class USCI_UART {
private:

protected:
	static void setupPins();
public:
	USCI_UART(){};
	~USCI_UART(){};
	static void enableRXInt(unsigned char enable);
	static void enableTXInt(unsigned char enable);
	static void setup(	unsigned char CTL0=0,unsigned char CTL1=UCSSEL1,
							unsigned char BR0=104,unsigned char BR1=0,
							unsigned char MCTL=UCBRS0);
	static void run(unsigned char (*TXCallback)(unsigned char volatile *value),
						unsigned char  (*RXCallback)(unsigned char value));
};

/********************************************************************************************************************/
class USCI_I2C {
protected:
	static void setupPins();
public:
	USCI_I2C(){};
	~USCI_I2C(){};
	static void enableRXInt(unsigned char enable);
	static void enableTXInt(unsigned char enable);
	static void enableSCInt(unsigned char mask);
	static void setupSlave(unsigned char (*SCallback)(unsigned char volatile *stat),
	                           unsigned char (*TCallback)(unsigned char volatile *value),
	                           unsigned char (*RCallback)(unsigned char value),
	                           unsigned char slave_address, unsigned char statflg);
	static void setupMaster(unsigned char prescale,
							unsigned char (*SCallback)(unsigned char volatile *stat),
		                    unsigned char (*TCallback)(unsigned char volatile *value),
		                    unsigned char (*RCallback)(unsigned char value),
		                    unsigned char statflg);
};

/********************************************************************************************************************/
class USCI_I2C_Master : public USCI_I2C {
private:
	static unsigned char byteCtr;
	static unsigned char multibyte;
	static unsigned char *i2c_Buffer;
	static unsigned char isByteBuffer;
	static ByteBuffer *i2c_ByteBuffer;
	static unsigned char rx_isr(unsigned char data);
	static unsigned char tx_isr(volatile unsigned char* port);
	static unsigned char sc_isr(volatile unsigned char* port);
	static void receive(void);
public:
	USCI_I2C_Master(){};
	~USCI_I2C_Master(){};
	static void setup(unsigned char prescale);
	static unsigned char isSlavePresent(unsigned char slave_address);
	static unsigned char isNotReady(){return BTS(UCB0STAT,UCBBUSY);}
	static void transmit(unsigned char slave,unsigned char byteCount,unsigned char *field);
	static void receive(unsigned char slave,unsigned char byteCount, unsigned char *field);
	static void transmit(unsigned char slave,ByteBuffer *field);
	static void receive(unsigned char slave,unsigned char byteCount, ByteBuffer *field);
};




#endif /* NUEVOUSCI_H_ */
