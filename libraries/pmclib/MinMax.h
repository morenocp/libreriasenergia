/*
 * MinMax.h
 *
 *  Created on: 1 ago. 2019
 *      Author: pedro
 */

#ifndef MINMAX_H_
#define MINMAX_H_

class MinMax
{
    float limitMax,limitMin;
    float minimum, maximum;
public:
    MinMax(float limMin,float limitMax);
    float getMin();
    float getMax();
    float getDelta();
    float getAverage();
    void update(float value);
    void reset();
    void reset(float limMin,float limitMax);
};

#endif /* MINMAX_H_ */
