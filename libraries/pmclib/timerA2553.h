/*
 * stTimerA2553.h
 *
 *  Created on: 20 abr. 2020
 *      Author: pedro
 */

#ifndef TIMERA2553_H_
#define TIMERA2553_H_
#ifdef __cplusplus
extern "C"
{
#endif

#include "stdint.h"
//
//#pragma pack(push, 1)
//typedef union {
//       uint16_t all;
//       struct {
//         uint16_t taIFG:1;
//         uint16_t taIE:1;
//         uint16_t taCLR:1;
//         uint16_t unused0:1;
//         uint16_t taMC:2;
//         uint16_t taID:2;
//         uint16_t taSSEL:2;
//         uint16_t unused1:6;
//       };
//} unTACTL;
//
//
//typedef union {
//    uint16_t all;
//    struct {
//        uint16_t taCCIFG:1;
//        uint16_t taCOV:1;
//        uint16_t taOUT:1;
//        uint16_t taCCI:1;
//        uint16_t taCCIE:1;
//        uint16_t taOUTMOD:3;
//        uint16_t taCAP:1;
//        uint16_t unused:1;
//        uint16_t taSCCI:1;
//        uint16_t taSCS:1;
//        uint16_t taCCIS:2;
//        uint16_t taCM:2;
//    };
//} unTACCTL;
//
//#pragma pack(pop)

typedef struct {
    volatile uint16_t taCTL;
    volatile uint16_t taCCTL[3];
    volatile uint16_t  unused[4];
    volatile uint16_t tar;
    volatile uint16_t taCCR[3];
} stRegsTIMERA;




typedef stRegsTIMERA* volatile ptRegsTimerA;


extern const ptRegsTimerA TimerA0;
extern const ptRegsTimerA TimerA1;


#ifdef __cplusplus
}
#endif
#endif /* TIMERA2553_H_ */
