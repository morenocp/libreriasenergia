class IPinOut{
  protected:
  unsigned char pin;
  public:
  virtual void set(){};
  virtual void clear(){};
  virtual void set(bool value){};
  virtual void toggle(){}; 
};

