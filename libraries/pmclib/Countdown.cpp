/*
 * Countdown.cpp
 *
 *  Created on: 20 abr. 2020
 *      Author: pedro
 */

#include <Countdown.h>

Countdown::Countdown(uint16_t counter,uint16_t initValue){
    this->init(counter,initValue);
};

void Countdown::init(uint16_t counter,uint16_t initValue){
    this->counter=counter;
    this->value=initValue;
}
bool Countdown::isZero(){
    bool ret=this->value==0;
    this->value=ret?this->counter:this->value-1;
    return ret;

}


