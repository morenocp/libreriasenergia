/*
 * DigitalPin.h
 *
 *  Created on: 21 abr. 2020
 *      Author: pedro
 */

#ifndef DIGITALPIN_H_
#define DIGITALPIN_H_
#include "ports2553.h"
#include "stdint.h"


class DigitalPin
{

protected:
    ptRegsPort port;
    uint8_t bit;
public:
    enum FUNCTION_PIN{
        IO_FUNCTION,
        PRIMARY_PERIPHERAL,
        RESERVED,
        SECONDARY_PERIPHERAL
    };

    DigitalPin(ptRegsPort port, uint8_t bit);
    void setDigitalMode(bool isDigital=true);
    void setFunctionPin(DigitalPin::FUNCTION_PIN mode,uint8_t* regSel2);
};

#endif /* DIGITALPIN_H_ */
