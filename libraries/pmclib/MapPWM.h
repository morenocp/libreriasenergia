/*
 * MapPWM.h
 *
 *  Created on: 24 jul. 2019
 *      Author: pedro
 */

#ifndef MAPPWM_H_
#define MAPPWM_H_
#include <LinearMap.h>

typedef struct {
    float x,y;
} Point2D;


class MapPWM
{
    LinearMap eqUp;
    LinearMap eqDown;
    Point2D points[4];
    float diffOffset;
    void updateDiffOffset();
    void updatePoint1();
public:
    MapPWM();
    MapPWM(float slopeUp, float offsetUp,float slopeDown,float offsetDown);
    float getSlope(bool isUp);
    float getOffset(bool isUp);
    float getDiffOffset(bool isUp);
    void setOffset(float offset,bool isUp);
    void setSlope(float value,bool isUp);
    float map(float x,bool isUp );
    float imap(float y,bool isUp );
    void setPoint2D(unsigned char n, float x, float y);
    void setPoint2D(unsigned char n, float value, bool isX);
    bool updateEqs();
    float delta(float x,bool isUp);
    float idelta(float y,bool isUp);
};

#endif /* MAPPWM_H_ */
