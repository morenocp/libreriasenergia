/*
 * PinIn.h
 *
 *  Created on: 24 abr. 2017
 *      Author: pedro
 */

#ifndef PININ_H_
#define PININ_H_

/* pulltype=0: disable pull resistance, 
   pulltype=1: pullup enable,
   pulltype=2: pulldown enable;
*/
class PinIn
{
    unsigned char pin;
    volatile unsigned char *port;
public:
    PinIn(const volatile unsigned char& port,unsigned char pin);
    PinIn(const volatile unsigned char& port,unsigned char pin,
                volatile unsigned char& portdir,
                volatile unsigned char& portren,
                volatile unsigned char& portout,unsigned char pulltype=1);
    bool isSet();
    bool isClear();
};

#endif /* PININ_H_ */
