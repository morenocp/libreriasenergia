/*
 * ports2553.c
 *
 *  Created on: 14 abr. 2020
 *      Author: pedro
 */

#include "ports2553.h"
#include <msp430.h>


const ptRegsPort Port1=(ptRegsPort)&P1IN;
const ptRegsPort Port2=(ptRegsPort)&P2IN;
const ptSel2Port PortSel2=(ptSel2Port)&P1SEL2;


