/*
 * CharBuffer.cpp
 *
 *  Created on: 20/06/2014
 *      Author: pedro
 */

#include "ByteBuffer.h"

/*
ByteBuffer::ByteBuffer(unsigned char size){
    buffer=(char*) malloc(size);
    head=0;
    tail=0;
    this->size=size;
}*/

ByteBuffer::ByteBuffer(unsigned char *buffer,unsigned char size){
    this->buffer=(char*)buffer;
    head=0;
    tail=0;
    this->size=size;

}

unsigned char ByteBuffer::isEmpty(){
        return (head==tail);
    }
unsigned char ByteBuffer::available(){
        return (size+head-tail)%size;
    }

unsigned char ByteBuffer::isFull(){
         unsigned char i=(head+1)<size?head+1:0;
         return i==tail;
    }

void ByteBuffer::push(char data){
        buffer[head]=data;
        head=++head<size?head:0;
    }
char ByteBuffer::pop(){
        char data=buffer[tail];
        tail=++tail<size?tail:0;
        return data;
    }
void ByteBuffer::clear(){tail=0;head=0;}

