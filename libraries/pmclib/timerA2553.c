/*
 * timerA2553.c
 *
 *  Created on: 20 abr. 2020
 *      Author: pedro
 */

#include "timerA2553.h"
#include "msp430.h"


const ptRegsTimerA TimerA0=(ptRegsTimerA)&TA0CTL;
const ptRegsTimerA TimerA1=(ptRegsTimerA)&TA1CTL;
