/*
 * unionsByte.h
 *
 *  Created on: 16 abr. 2020
 *      Author: pedro
 */

#ifndef UNIONSBYTE_H_
#define UNIONSBYTE_H_

#ifdef __cplusplus
extern "C"
{
#endif
#pragma pack(push,1)
typedef union {
       uint8_t all;
       struct {
         uint8_t bit0:1;
         uint8_t bit1:1;
         uint8_t bit2:1;
         uint8_t bit3:1;
         uint8_t bit4:1;
         uint8_t bit5:1;
         uint8_t bit6:1;
         uint8_t bit7:1;
       };
} unByte;


typedef union {
     uint16_t all;
    struct{
         uint8_t lsbyte;
         uint8_t msbyte;
    };
    struct {
         uint8_t bit0:1;
         uint8_t bit1:1;
         uint8_t bit2:1;
         uint8_t bit3:1;
         uint8_t bit4:1;
         uint8_t bit5:1;
         uint8_t bit6:1;
         uint8_t bit7:1;
         uint8_t bit8:1;
         uint8_t bit9:1;
         uint8_t bit10:1;
         uint8_t bit11:1;
         uint8_t bit12:1;
         uint8_t bit13:1;
         uint8_t bit14:1;
         uint8_t bit15:1;
    };
} unWord;


typedef union {
    uint32_t uLong;
    int32_t  sLong;
    float    Float;
    uint16_t words[2];
    uint8_t  bytes[4];
} unMultiFormat;

#pragma pack(pop)

#ifdef __cplusplus
}
#endif


#endif /* UNIONSBYTE_H_ */
