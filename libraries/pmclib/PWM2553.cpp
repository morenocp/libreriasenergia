/*
 * PWM2553.cpp
 *
 *  Created on: 20 abr. 2020
 *      Author: pedro
 */

#include <PWM2553.h>
#include "msp430.h"

PWM2553::PWM2553(ptRegsTimerA timer, uint8_t reg)
{
    this->reg=reg;
    this->timer=timer;
}

    void PWM2553::init(uint16_t ccr0,uint16_t ctl,uint16_t cctl){
        this->timer->taCCR[0]=ccr0;
        this->timer->taCCTL[this->reg]|=cctl;
        this->backCTL=ctl;
        this->clear();
    }
    void PWM2553::clear(){
        this->timer->taCTL|=TACLR; //BIS(TA1CTL, TACLR);
        this->timer->taCTL|=(this->backCTL&0x3c0); //BIS(TA1CTL, TASSEL_2 | ID_3);
        this->timer->taCTL|=(this->backCTL&0x30); //BIS(TA1CTL, MC_1)
    }
    void PWM2553::setDuty(uint16_t duty){
        this->timer->taCCR[this->reg]=duty;
    }
