/*
 * LinearMap.h
 *
 *  Created on: 24 jul. 2019
 *      Author: pedro
 */

#ifndef LINEARMAP_H_
#define LINEARMAP_H_

class LinearMap
{
private:
    float slope,offset;
public:
    LinearMap();
    LinearMap(float varSlope,float varOffset);
    float getSlope();
    float getOffset();
    bool update(float varSlope,float varOffset);
    bool update (float x1, float y1, float x2,float y2);
    float map(float x);
    float imap (float y);
    float delta(float x);
    float idelta(float y);
    bool setSlope(float varSlope);
    void setOffset(float varOffset);
};

#endif /* LINEARMAP_H_ */
