/*
 * PWM2553.h
 *
 *  Created on: 20 abr. 2020
 *      Author: pedro
 */

#ifndef PWM2553_H_
#define PWM2553_H_
#include "timerA2553.h"


class PWM2553
{
private:
    uint8_t reg;
    uint16_t backCTL;
    ptRegsTimerA timer;
public:
    PWM2553(ptRegsTimerA timer, uint8_t reg=1);
    void init(uint16_t ccr0,uint16_t ctl,uint16_t cctl);
    void clear();
    void setDuty(uint16_t duty);
};

#endif /* PWM2553_H_ */
