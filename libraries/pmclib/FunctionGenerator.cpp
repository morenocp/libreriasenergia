/*
 * FunctionGenerator.cpp
 *
 *  Created on: 25 jul. 2019
 *      Author: pedro
 */

#include <FunctionGenerator.h>

FunctionGenerator::FunctionGenerator(const float *data, unsigned int ndata)
{
    tableData=data;
    nData=ndata;
    reset();
}

void FunctionGenerator::nextPhase(){
    phase=++phase<nData?phase:0;
}


float FunctionGenerator::getData(){
    return tableData[phase];
}

unsigned int FunctionGenerator::getPhase(){
    return phase;
}

void FunctionGenerator::reset(){
    phase=0;

}
