/*
 * lpf5.h
 *
 *  Created on: 23/02/2015
 *      Author: pedro
 */

#ifndef LPF5529_H_
#define LPF5529_H_

#include <msp430.h>
#include "bitman.h"

//Definiciones para el led1 Rojo
#define ledRed  			BIT0
#define ledRedSetup()  		BIS(P1DIR,ledRed)
#define ledRedOn()  		BIS(P1OUT,ledRed)
#define ledRedOff()  		BIC(P1OUT,ledRed)
#define ledRedTgl()  		BIX(P1OUT,ledRed)

//Definiciones para el led2 Verde
#define ledGreen 			BIT7
#define ledGreenSetup()  	BIS(P4DIR,ledGreen)
#define ledGreenOn()  		BIS(P4OUT,ledGreen)
#define ledGreenOff()  		BIC(P4OUT,ledGreen)
#define ledGreenTgl()  		BIX(P4OUT,ledGreen)

//Definiciones para los 2 led
#define ledsSetup()  		st(ledRedSetup(); ledGreenSetup();)
#define ledsOn()  			st(ledRedOn(); ledGreenOn(); )
#define ledsOff()  			st(ledRedOff(); ledGreenOff();)
#define ledsTgl() 			st(ledRedTgl(); ledGreenTgl();)


//Definiciones para el boton s1
#define btnS1Bit 			BIT1
#define btnS1Setup() 		st(BIC(P2DIR,btnS1Bit);BIS(P2REN,btnS1Bit);BIS(P2OUT,btnS1Bit);)
#define btnS1IsUp() 		(P2IN & btnS1Bit)
#define btnS1IsDown() 		!btnS1IsUp()
#define btnS1IE_ON() 		st(BIS(P2IE,btnS1Bit); BIS(P2IES,btnS1Bit);)
#define btnS1RETI()  		BIC(P2IFG,btnS1Bit)

//Definiciones para el boton s2
#define btnS2Bit 			BIT1
#define btnS2Setup() 		st(BIC(P1DIR,btnS2Bit);BIS(P1REN,btnS2Bit);BIS(P1OUT,btnS2Bit);)
#define btnS2IsUp() 		(P1IN & btnS2Bit)
#define btnS2IsDown() 		!btnS2IsUp()
#define btnS2IE_ON() 		st(BIS(P1IE,btnS2Bit); BIS(P1IES,btnS2Bit);)
#define btnS2RETI() 		BIC(P1IFG,btnS2Bit)



/* Use el vector del puerto para manejar la interrupcion, ejemplo:
 *
 #pragma vector= PORT1_VECTOR
__interrupt void isr_puerto1(void){
	Inserte su código aqui
	btnS2RETI(); //limpia la bandera de interrupcion
}
 */

//Definiciones para calibrar el DCO en el msp430f5529 mediante el ajuste de DCOCLKDIV
#define dclockDivSetup(CTL1,CTL2,CTL3,DELAY) st(		\
	_BIS_SR(SCG0);										\
   UCSCTL0=0;											\
   UCSCTL1|=CTL1; 										\
   UCSCTL2|=CTL2;										\
   UCSCTL3|=CTL3; 										\
   _BIC_SR(SCG0);										\
	volatile unsigned int x=DELAY;					\
	while(x--) __delay_cycles(1024); )


#define clockSetup1MHz() 	dclockDivSetup(DCORSEL_3,30,SELREF_2,31)
#define clockSetup8MHz() 	dclockDivSetup(DCORSEL_6,243,SELREF_2,244)
#define clockSetup12MHz() 	dclockDivSetup(DCORSEL_7,365,SELREF_2,366)
#define clockSetup16MHz() 	dclockDivSetup(DCORSEL_7,487,SELREF_2,488)
#define clockSetup20MHz() 	dclockDivSetup(DCORSEL_7,609,SELREF_2,610)
//#define clockSetup25MHz 	clockSetup(DCORSEL_7,762,SELREF_2)
//#define clockSetup30MHz 	clockSetup(DCORSEL_7,915,SELREF_2)


//Definiciones varias

#define GIE_ON()  			_BIS_SR(GIE)	//Habilita la interrupcion general
#define NMIE_ON()  			st(BIS(SFRRPCR,0x0f);BIS(SFRIE1,NMIIE);)
#define NMIE_RETI() 		st(BIC(SFRIFG1,NMIIFG);NMIE_ON();)
#define SMCLK_SHOW() 		st(BIS(P2DIR,BIT2); BIS(P2SEL,BIT2);)  //selecciona la funcion periferica SMCLK

/*
#pragma vector= UNMI_VECTOR   //vector de interrupcion no enmascarable del usuario
__interrupt void nmi_isrvoid){   //rutina de servicio
NMIE_RETI();
}
*/






//Definiciones para el WDT
#define WDTI_ON() 			BIS(SFRIE1,WDTIE);
#define WDT_1SEG() 			WDTPW|WDTTMSEL|WDTSSEL0|WDTCNTCL|WDTIS2		//WD modo intervalos de tiempo, fuente=ACLK/32768
#define WDT_1Hz() 			WDTPW|WDTTMSEL|WDTSSEL0|WDTCNTCL|WDTIS2					//WD modo intervalos de tiempo, fuente=ACLK/32768
#define WDT_4Hz() 			WDTPW|WDTTMSEL|WDTSSEL0|WDTCNTCL|WDTIS2|WDTIS0					//WD modo intervalos de tiempo, fuente=ACLK/8192
#define WDT_64Hz() 			WDTPW|WDTTMSEL|WDTSSEL0|WDTCNTCL|WDTIS2|WDTIS1			//WD modo intervalos de tiempo, fuente=ACLK/512
#define WDT_512Hz() 		WDTPW|WDTTMSEL|WDTSSEL0|WDTCNTCL|WDTIS2|WDTIS1|WDTIS0	//WD modo intervalos de tiempo, fuente=ACLK/64

/*
#pragma vector= WDT_VECTOR //vector de interrupcion del WDT
__interrupt void isr_wdt(void){ //rutina de servicio
}
*/


#endif /* LPF5529_H_ */
