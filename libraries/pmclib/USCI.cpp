/*
 * NuevoUSCI.cpp
 *
 *  Created on: 03/02/2015
 *      Author: pedro
 */



#if defined (__MSP430G2553__)
#include <USCI.h>

/********************************************************************************************************************/
//Codigo correspondiente a USCIVector
unsigned char (* USCIVector::ucaRxFunction)(unsigned char)=0;
unsigned char (* USCIVector::ucaTxFunction)(volatile unsigned char*)=0;
unsigned char (* USCIVector::ucbRxFunction)(unsigned char)=0;
unsigned char (* USCIVector::ucbTxFunction)(volatile unsigned char*)=0;
unsigned char (* USCIVector::i2cSCFunction)(volatile unsigned char*)=0;
unsigned char  USCIVector::i2cMode=0;

/*
#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCIVector:: USCIAB0RX_ISR(void)
{
	if(ucaRxFunction && BTS(IFG2,UCA0RXIFG )){
		if(ucaRxFunction(UCA0RXBUF)) LPM4_EXIT;	//Llama a la funcion y si regresa 1, salir de modo de bajo consumo
	} else

	if(i2cMode ){
		if(i2cSCFunction && BTS(UCB0STAT,0x0f))
			if(i2cSCFunction(&UCB0STAT)) LPM4_EXIT;	//Llama a la funcion y si regresa 1, salir de modo de bajo consumo
	} else
		if(ucbRxFunction && BTS(IFG2,UCB0RXIFG ))
			if(ucbRxFunction(UCB0RXBUF)) LPM4_EXIT;	//Llama a la funcion y si regresa 1, salir de modo de bajo consumo

}



#pragma vector = USCIAB0TX_VECTOR
__interrupt void USCIVector::USCIAB0TX_ISR(void)
{
	if(ucaTxFunction && BTS(IFG2,UCA0TXIFG )){
		if(ucaTxFunction(&UCA0TXBUF)) LPM4_EXIT; //Llama a la funcion y si regresa 1, salir de modo de bajo consumo
	} else

	if(i2cMode){
		if(ucbRxFunction && BTS(IFG2,UCB0RXIFG )){
			if(ucbRxFunction(UCB0RXBUF)) LPM4_EXIT;	//Llama a la funcion y si regresa 1, salir de modo de bajo consumo

		} else if(ucbTxFunction && BTS(IFG2,UCB0TXIFG ))
			if(ucbTxFunction(&UCB0TXBUF)) LPM4_EXIT;	//Llama a la funcion y si regresa 1, salir de modo de bajo consumo


	} else if(ucbTxFunction && BTS(IFG2,UCB0TXIFG ))
				if(ucbTxFunction(&UCB0TXBUF)) LPM4_EXIT;	//Llama a la funcion y si regresa 1, salir de modo de bajo consumo


}

*/
/********************************************************************************************************************/
//Codigo correspondiente a USCI_UART

void USCI_UART::setupPins(){
		BIS(P1OUT,uartTX|uartRX);
		BIS(P1DIR,uartTX);
		BIC(P1DIR,uartRX);
		BIS(P1SEL,uartRX|uartTX); //Selecciona funcion periferica de RX y TX
		BIS(P1SEL2,uartRX|uartTX);
	}

void USCI_UART::enableRXInt(unsigned char enable){
	if(enable) BIS(IE2,UCA0RXIE); //Habilita la generacion de int para recibir
					else BIC(IE2,UCA0RXIE);
		}

void USCI_UART::enableTXInt(unsigned char enable){
		if(enable) BIS(IE2,UCA0TXIE); //Habilita la generacion de int para transmitir
		else BIC(IE2,UCA0TXIE);
		}

void USCI_UART::setup(unsigned char CTL0,unsigned char CTL1,
				    unsigned char BR0,unsigned char BR1,
				    unsigned char MCTL){
			BIS(UCA0CTL1,UCSWRST);
			setupPins();
			UCA0CTL0=CTL0; //8 bits, 1 stop, no paridad, asincrono
			BIS(UCA0CTL1,CTL1);                     // fuente =SMCLK
			UCA0BR0 = BR0;                            // 1MHz 9600
			UCA0BR1 = BR1;                              // 1MHz 9600
			BIS(UCA0MCTL,MCTL);                        // Modulation UCBRSx = 1
			BIC(UCA0CTL1,UCSWRST); //Se sale del modo reset;
		}

void USCI_UART::run(unsigned char (*TXCallback)(unsigned char volatile *value),
			unsigned char  (*RXCallback)(unsigned char value)){
		if(TXCallback){
			USCIVector::setUCATXFunction(TXCallback);
			enableTXInt(1);
		} else enableTXInt(0);
		if(RXCallback){
			USCIVector::setUCARXFunction(RXCallback);
			enableRXInt(1);
		} else enableRXInt(0);

	}

/********************************************************************************************************************/
//Codigo correspondiente a USCI_I2C

void USCI_I2C::setupPins(){
	BIS(P1SEL,i2cSDA|i2cSCL); //Selecciona funcion periferica de RX y TX
	BIS(P1SEL2,i2cSDA|i2cSCL);
}

void USCI_I2C::enableRXInt(unsigned char enable){
	if(enable) BIS(IE2,UCB0RXIE); //Habilita la generacion de int para recibir
					else BIC(IE2,UCB0RXIE);
		}

void USCI_I2C::enableTXInt(unsigned char enable){
		if(enable) BIS(IE2,UCB0TXIE); //Habilita la generacion de int para transmitir
		else BIC(IE2,UCB0TXIE);
		}

void USCI_I2C::enableSCInt(unsigned char mask){
		BIS(UCB0I2CIE,mask); //Habilita la generacion de int para transmitir
		}

void USCI_I2C::setupSlave(unsigned char (*SCallback)(unsigned char volatile *stat),
                           unsigned char (*TCallback)(unsigned char volatile *value),
                           unsigned char (*RCallback)(unsigned char value),
                           unsigned char slave_address, unsigned char statflg){
	BIS(UCB0CTL1,UCSWRST);                      // Enable SW reset
	setupPins();               				// Assign I2C pins to USCI_B0
	BIS(UCB0CTL0,UCMODE_3 + UCSYNC);            // I2C Slave, synchronous mode
	UCB0I2COA = slave_address;                	// set own (slave) address
	USCIVector::setI2CMode(1);
	USCIVector::setI2CSCFunction(SCallback);
	USCIVector::setUCBRXFunction(RCallback);
	USCIVector::setUCBTXFunction(TCallback);
	BIC(UCB0CTL1,UCSWRST);                     	// Clear SW reset, resume operatio
	BIC(IFG2,UCB0TXIFG+UCB0RXIFG);
	if(TCallback) enableTXInt(1); else enableTXInt(0);
	if(RCallback) enableRXInt(1); else enableRXInt(0);
	BIC(UCB0STAT,0x0f);
	enableSCInt(statflg);                     // Enable STT interrupt
}

void USCI_I2C::setupMaster(unsigned char prescale,
		unsigned char (*SCallback)(unsigned char volatile *stat),
        unsigned char (*TCallback)(unsigned char volatile *value),
        unsigned char (*RCallback)(unsigned char value),
        unsigned char statflg){

	BIS(UCB0CTL1,UCSWRST);                		// Enable SW reset
	setupPins();								// Assign I2C pins to USCI_B0
	BIS(UCB0CTL0,UCMST + UCMODE_3 + UCSYNC);    // I2C Master, synchronous mode
	BIS(UCB0CTL1,UCSSEL_2 + UCSWRST);           // Use SMCLK, keep SW reset
	UCB0BR0 = prescale;                         // set prescaler
	UCB0BR1 = 0;
	USCIVector::setI2CMode(1);
	USCIVector::setI2CSCFunction(SCallback);
	USCIVector::setUCBRXFunction(RCallback);
	USCIVector::setUCBTXFunction(TCallback);
	enableSCInt(statflg);
	BIC(UCB0CTL1,UCSWRST);                      // Clear SW reset, resume operation
	BIC(IFG2,UCB0TXIFG+UCB0RXIFG);

}

/********************************************************************************************************************/
//Codigo correspondiente a USCI_I2C_Master

unsigned char USCI_I2C_Master::byteCtr=0;
unsigned char USCI_I2C_Master::multibyte=0;
unsigned char* USCI_I2C_Master::i2c_Buffer=0;
unsigned char USCI_I2C_Master::isByteBuffer=0;
ByteBuffer *USCI_I2C_Master::i2c_ByteBuffer=0;

void USCI_I2C_Master::setup(unsigned char prescale){

	USCI_I2C::setupMaster(prescale,&sc_isr,&tx_isr,&rx_isr,UCNACKIE);
}

void USCI_I2C_Master::receive(void){
	BIC(UCB0CTL1,UCTR);
		enableRXInt(1); 						// Enable RX interrupt
		enableTXInt(0);

		if ( byteCtr == 1 ){
			multibyte=0;
			__disable_interrupt();
			BIS(UCB0CTL1,UCTXSTT);                      // I2C start condition
			while (BTS(UCB0CTL1,UCTXSTT));               // Start condition sent?
			BIS(UCB0CTL1,UCTXSTP);                      // I2C stop condition
			__enable_interrupt();
	  } else if ( byteCtr > 1 ) {
		  	 multibyte=1;
		  	 BIS(UCB0CTL1,UCTXSTT);                      // I2C start condition
	  } ;
}
void USCI_I2C_Master::receive(unsigned char slave,unsigned char byteCount, unsigned char *field){
	UCB0I2CSA = slave;
	i2c_Buffer = field;
	byteCtr=byteCount;
	isByteBuffer=0;
	receive();

}

void USCI_I2C_Master::receive(unsigned char slave,unsigned char byteCount, ByteBuffer *field){
	UCB0I2CSA = slave;
	i2c_ByteBuffer = field;
	byteCtr=byteCount;
	isByteBuffer=1;
	receive();

}

void USCI_I2C_Master::transmit(unsigned char slave,unsigned char byteCount,unsigned char *field){
	UCB0I2CSA = slave;
	i2c_Buffer = field;
	byteCtr = byteCount;
	isByteBuffer=0;
	enableTXInt(1); 						// Enable TX interrupt
	enableRXInt(0);
	BIS(UCB0CTL1,UCTR + UCTXSTT);                 // I2C TX, start condition
}

void USCI_I2C_Master::transmit(unsigned char slave,ByteBuffer *field){
	UCB0I2CSA = slave;
	i2c_ByteBuffer = field;
	byteCtr = field->available();
	isByteBuffer=1;
	enableTXInt(1); 						// Enable TX interrupt
	enableRXInt(0);
	BIS(UCB0CTL1,UCTR + UCTXSTT);                 // I2C TX, start condition
}

unsigned char USCI_I2C_Master::rx_isr(unsigned char data){
	if(isByteBuffer){
		i2c_ByteBuffer->push(data);
	} else{
		*i2c_Buffer=data;				//Guarda el dato en el buffer
		i2c_Buffer++;
	}

	byteCtr--;
	if(byteCtr==0) enableRXInt(0); else
		  if(multibyte && byteCtr==1) BIS(UCB0CTL1,UCTXSTP);
	return 0;
}


unsigned char USCI_I2C_Master::tx_isr(volatile unsigned char* port){
	if (byteCtr == 0){
	    	BIS(UCB0CTL1,UCTXSTP);                    // I2C stop condition
	        BIC(IFG2,UCB0TXIFG);                     // Clear USCI_B0 TX int flag
	        enableTXInt(0); 						// Disable TX interrupt
	    }
	    else {
	    	if(isByteBuffer){
	    		*port = i2c_ByteBuffer->pop();
	    	} else
	    	{
	    		*port = *i2c_Buffer;
	    		 i2c_Buffer++;
	    	}
	      byteCtr--;
	    }
	return 0;
}


unsigned char USCI_I2C_Master::sc_isr(volatile unsigned char* port){
	if (BTS(*port,UCNACKIFG)){            // send STOP if slave sends NACK
	    BIS(UCB0CTL1,UCTXSTP);
	    BIC(*port,UCNACKIFG);
	  }
	return 0;
}

unsigned char USCI_I2C_Master::isSlavePresent(unsigned char slave_address){
  unsigned char ie2_bak, slaveadr_bak, ucb0i2cie, returnValue,sr;
  sr=READ_SR;
  //sr=__get_SR_register();
  ucb0i2cie = UCB0I2CIE;                      // restore old UCB0I2CIE
  ie2_bak = IE2;                              // store IE2 register
  slaveadr_bak = UCB0I2CSA;                   // store old slave address
  BIC(UCB0I2CIE,UCNACKIE);                    // no NACK interrupt
  UCB0I2CSA = slave_address;                  // set slave address
  BIC(IE2,UCB0TXIE + UCB0RXIE);              // no RX or TX interrupts
  __disable_interrupt();
  BIS(UCB0CTL1,UCTR + UCTXSTT + UCTXSTP);       // I2C TX, start condition
  while (BTS(UCB0CTL1,UCTXSTP));                 // wait for STOP condition
  returnValue = !(UCB0STAT & UCNACKIFG);
  if(BTS(sr,GIE)) __enable_interrupt();
  IE2 = ie2_bak;                              // restore IE2
  UCB0I2CSA = slaveadr_bak;                   // restore old slave address
  UCB0I2CIE = ucb0i2cie;                      // restore old UCB0CTL1
  return returnValue;                         // return whether or not
                                              // a NACK occured
}
#endif
