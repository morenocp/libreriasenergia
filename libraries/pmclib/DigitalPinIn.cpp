/*
 * DigitalPinIn.cpp
 *
 *  Created on: 21 abr. 2020
 *      Author: pedro
 */

#include <DigitalPinIn.h>
#include "bitman.h"

DigitalPinIn::DigitalPinIn(ptRegsPort port, uint8_t bit,DigitalPinIn::INPUT_MODE  mode):
DigitalPin(port,bit)
{
    this->config(mode);

}



bool DigitalPinIn::isSet(){
    if (BTS(this->port->in,this->bit))
    return true;
    else
        return false;
}

bool DigitalPinIn::isClear(){
    return not(isSet());
};

bool DigitalPinIn::isEnableInt(){
    if(BTS(this->port->ie,this->bit))
        return true;
    else return false;
};

void DigitalPinIn::enableInt(bool enable){
    if (enable) BIS(this->port->ie,this->bit);
    else
        BIC(this->port->ie,this->bit);
};

void DigitalPinIn::clearIntFlag(){
   BIC(this->port->ifg,this->bit);
};

bool DigitalPinIn::isIntFlag(){
    if(BTS(this->port->ifg,this->bit))
        return true;
    else return false;
};

void DigitalPinIn::setIntEdge(bool hi2low){
    if(hi2low) BIS(this->port->ies,this->bit);
    else
        BIS(this->port->ies,this->bit);
};

void DigitalPinIn::config(DigitalPinIn::INPUT_MODE mode){
    switch(mode){
    case INPUT_NO_RESISTOR:
        BIC(this->port->ren,this->bit);
        break;
    case INPUT_RES_PULLUP:
        BIS(this->port->ren,this->bit);
        BIS(this->port->out,this->bit);
        break;
    case INPUT_RES_PULLDOWN:
        BIS(this->port->ren,this->bit);
        BIC(this->port->out,this->bit);
        break;
    }
}
