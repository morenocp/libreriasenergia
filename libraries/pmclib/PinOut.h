
/*
 * PinOut.h
 *
 *  Created on: 22/03/2017
 *      Author: pedro
 */

#ifndef PINOUT_H_
#define PINOUT_H_
//#include "IPinOut.h"

class PinOut {
  volatile unsigned char *port;
  unsigned char pin;
  public:
  PinOut(volatile unsigned char &port, unsigned char pin,unsigned char initvalue=0);
  PinOut(volatile unsigned char &port, unsigned char pin, volatile unsigned char &portdir,unsigned char initvalue=0); 
  virtual void set();
  virtual void clear();
  virtual void set(bool value);
  virtual void toggle(); 
};


#endif /* PINOUT_H_ */

