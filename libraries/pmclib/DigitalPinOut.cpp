/*
 * DigitalPinOut.cpp
 *
 *  Created on: 21 abr. 2020
 *      Author: pedro
 */

#include <DigitalPinOut.h>
#include "bitman.h"

DigitalPinOut::DigitalPinOut(ptRegsPort port, uint8_t bit, bool isSet):
DigitalPin(port,bit)
{
   this->set(isSet);
   this->config();
}

void DigitalPinOut::set(){
    BIS(this->port->out,this->bit);
}
void DigitalPinOut::set(bool val){
    if(val) set(); else clear();
}
void DigitalPinOut::clear(){
    BIC(this->port->out,this->bit);
}

void DigitalPinOut::config(){
    BIS(this->port->dir,this->bit);
}
void DigitalPinOut::toogle(){
    BIX(this->port->out,this->bit);
}



