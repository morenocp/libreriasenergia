/*
 * MapPWM.cpp
 *
 *  Created on: 24 jul. 2019
 *      Author: pedro
 */

#include <MapPWM.h>

MapPWM::MapPWM()
{
  eqUp.update(1.0,0.0);
  eqDown.update(1.0,0.0);
  updateDiffOffset();
}


MapPWM::MapPWM(float slopeUp, float offsetUp,float slopeDown,float offsetDown){
    eqUp.update(slopeUp, offsetUp);
    eqDown.update(slopeDown, offsetDown);
    updateDiffOffset();
}

float MapPWM::getSlope(bool isUp){
    if (isUp) return eqUp.getSlope();
    else   return eqDown.getSlope();
}

float MapPWM::getOffset(bool isUp){
    if(isUp) return eqUp.getOffset();
    else return eqDown.getOffset();
}

float MapPWM::getDiffOffset(bool isUp){
    return isUp?0:diffOffset;
}

void MapPWM::setOffset(float offset,bool isUp){
    if(isUp) eqUp.setOffset(offset);
    else eqDown.setOffset(offset);
    updateDiffOffset();
}

void MapPWM::setSlope(float value,bool isUp){
    if(isUp) eqUp.setSlope(value);
    else eqDown.setSlope(value);
}

float MapPWM::map(float x,bool isUp ){
    if (isUp) return eqUp.map(x);
    else return eqDown.map(x);
}
float MapPWM::imap(float y,bool isUp ){
    if (isUp) return eqUp.imap(y);
    else return eqDown.imap(y);
}

float MapPWM::delta(float x,bool isUp ){
    if (isUp) return eqUp.delta(x);
    else return eqDown.delta(x);
}
float MapPWM::idelta(float y,bool isUp ){
    if (isUp) return eqUp.idelta(y);
    else return eqDown.idelta(y);
}


void MapPWM::setPoint2D(unsigned char n, float x, float y){
    if(n<4){
        points[n].x=x;
        points[n].y=y;
    }
}

void MapPWM::setPoint2D(unsigned char n, float value, bool isX){
    if(n<4){
        if(isX) points[n].x=value;
        else points[n].y=value;
    }
}

bool  MapPWM::updateEqs(){
    bool result=eqUp.update(points[0].x,points[0].y,points[1].x,points[1].y);
    result=eqDown.update(points[2].x,points[2].y,points[3].x,points[3].y)||result;
    if (result) {
        //updatePoint1();
        updateDiffOffset();
    }
    return result;

}
void MapPWM::updateDiffOffset(){
    diffOffset=eqUp.getOffset()-eqDown.getOffset();
}

void MapPWM::updatePoint1(){
    points[0].x=points[2].x;
    points[0].y=points[2].y;
}
