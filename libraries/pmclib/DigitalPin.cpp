/*
 * DigitalPin.cpp
 *
 *  Created on: 21 abr. 2020
 *      Author: pedro
 */

#include <DigitalPin.h>
#include <bitman.h>

DigitalPin::DigitalPin(ptRegsPort port, uint8_t bit)
{
    this->port=port;
    this->bit=bit;
    this->setDigitalMode(true);

}
void DigitalPin::setDigitalMode(bool isDigital){
    if(isDigital)
        this->setFunctionPin(IO_FUNCTION,0);
    else
        setFunctionPin(PRIMARY_PERIPHERAL,0);

}
void DigitalPin::setFunctionPin(DigitalPin::FUNCTION_PIN mode,uint8_t* regSel2){
    switch(mode){
    case IO_FUNCTION:
        BIC(this->port->sel,this->bit);
        if(regSel2) BIC((*regSel2),this->bit);
        break;
    case PRIMARY_PERIPHERAL:
        BIS(this->port->sel,this->bit);
        if(regSel2) BIC((*regSel2),this->bit);
        break;

    case RESERVED:
        BIC(this->port->sel,this->bit);
        if(regSel2) BIS((*regSel2),this->bit);
        break;
    case SECONDARY_PERIPHERAL:
        BIS(this->port->sel,this->bit);
        if(regSel2) BIS((*regSel2),this->bit);
        break;
    }
}
