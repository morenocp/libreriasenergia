/*
 * Countdown.h
 *
 *  Created on: 20 abr. 2020
 *      Author: pedro
 */

#ifndef COUNTDOWN_H_
#define COUNTDOWN_H_
#include "stdint.h"

class Countdown
{
private:
    uint16_t counter;
    uint16_t value;
public:
    Countdown(uint16_t counter=0,uint16_t initValue=0);
    void init(uint16_t counter,uint16_t initValue);
    bool isZero();
};

#endif /* COUNTDOWN_H_ */
