/*
 Print.cpp - Base class that provides print() and println()
 Copyright (c) 2008 David A. Mellis.  All right reserved.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 Modified 23 November 2006 by David A. Mellis
 Modified for msp403 2012 by Robert Wessels
 */

//#include <stdlib.h>
//#include <stdio.h>
//#include <string.h>
//#include <math.h>

#include <mPrint.h>

// Public Methods //////////////////////////////////////////////////////////////

/* default implementation: may be overridden */
unsigned int mPrint::write(const unsigned char *buffer, unsigned int size)
{
  unsigned int n = 0;
  while (size--) {
    n += write(*buffer++);
  }
  return n;
}
/*
unsigned int mPrint::print(const String &s)
{
  unsigned int n = 0;
  for (uint16_t i = 0; i < s.length(); i++) {
    n += write(s[i]);
  }
  return n;
}*/

unsigned int mPrint::print(const char str[])
{
  return write(str);
}

unsigned int mPrint::print(char c)
{
  return write(c);
}

unsigned int mPrint::print(unsigned char b, int base)
{
  return print((unsigned long) b, base);
}

unsigned int mPrint::print(int n, int base)
{
  return print((long) n, base);
}

unsigned int mPrint::print(unsigned int n, int base)
{
  return print((unsigned long) n, base);
}

unsigned int mPrint::print(long n, int base)
{
  if (base == 0) {
    return write(n);
  } else if (base == 10) {
    if (n < 0) {
      int t = print('-');
      n = -n;
      return printNumber(n, 10) + t;
    }
    return printNumber(n, 10);
  } else {
    return printNumber(n, base);
  }
}

unsigned int mPrint::print(unsigned long n, int base)
{
  if (base == 0) return write(n);
  else return printNumber(n, base);
}

unsigned int mPrint::print(double n, int digits)
{
  return printFloat(n, digits);
}

//unsigned int mPrint::println(const __FlashStringHelper *ifsh)
//{
//  unsigned int n = print(ifsh);
//  n += println();
//  return n;
//}
/*
unsigned int mPrint::print(const Printable& x)
{
  return x.printTo(*this);
}*/

unsigned int mPrint::println(void)
{
  unsigned int n = print('\r');
  n += print('\n');
  return n;
}

/*
unsigned int mPrint::println(const String &s)
{
  unsigned int n = print(s);
  n += println();
  return n;
}*/

unsigned int mPrint::println(const char c[])
{
  unsigned int n = print(c);
  n += println();
  return n;
}

unsigned int mPrint::println(char c)
{
  unsigned int n = print(c);
  n += println();
  return n;
}

unsigned int mPrint::println(unsigned char b, int base)
{
  unsigned int n = print(b, base);
  n += println();
  return n;
}

unsigned int mPrint::println(int num, int base)
{
  unsigned int n = print(num, base);
  n += println();
  return n;
}

unsigned int mPrint::println(unsigned int num, int base)
{
  unsigned int n = print(num, base);
  n += println();
  return n;
}

unsigned int mPrint::println(long num, int base)
{
  unsigned int n = print(num, base);
  n += println();
  return n;
}

unsigned int mPrint::println(unsigned long num, int base)
{
  unsigned int n = print(num, base);
  n += println();
  return n;
}

unsigned int mPrint::println(double num, int digits)
{
  unsigned int n = print(num, digits);
  n += println();
  return n;
}
/*
unsigned int mPrint::println(const Printable& x)
{
  unsigned int n = print(x);
  n += println();
  return n;
}*/

// Private Methods /////////////////////////////////////////////////////////////

unsigned int mPrint::printNumber(unsigned long n, unsigned char base) {
  char buf[8 * sizeof(long) + 1]; // Assumes 8-bit chars plus zero byte.
  char *str = &buf[sizeof(buf) - 1];

  *str = '\0';

  // prevent crash if called with base == 1
  if (base < 2) base = 10;

  do {
    unsigned long m = n;
    n /= base;
    char c = m - base * n;
    *--str = c < 10 ? c + '0' : c + 'A' - 10;
  } while(n);

  return write(str);
}

unsigned int mPrint::printFloat(double number, unsigned char digits)
{
  unsigned int n = 0;

  // Handle negative numbers
  if (number < 0.0)
  {
     n += print('-');
     number = -number;
  }

  // Round correctly so that print(1.999, 2) prints as "2.00"
  double rounding = 0.5;
  for (unsigned char i=0; i<digits; ++i)
    rounding /= 10.0;

  number += rounding;

  // Extract the integer part of the number and print it
  unsigned long int_part = (unsigned long)number;
  double remainder = number - (double)int_part;
  n += print(int_part);

  // Print the decimal point, but only if there are digits beyond
  if (digits > 0) {
    n += print(".");
  }

  // Extract digits from the remainder one at a time
  while (digits-- > 0)
  {
    remainder *= 10.0;
    int toPrint = int(remainder);
    n += print(toPrint);
    remainder -= toPrint;
  }

  return n;
}


unsigned int mPrint:: printUInt(unsigned int data){ //Muestra data en formato decimal
	unsigned int divisor=10000;
	while(divisor){
		print((char)(data/divisor+0x30));
		data%=divisor;
		divisor/=10;
	}
	return 5;
}


unsigned int mPrint:: printUIntDP(unsigned int data,unsigned char dp){ //Muestra data en formato decimal
	unsigned int divisor=10000;
	unsigned char digito=5;
	while(divisor){
		digito--;
		print((char)(data/divisor+0x30));
		if(digito==dp) print('.');
		data%=divisor;
		divisor/=10;
	}
	return 6;
}


unsigned int mPrint:: printUChar(unsigned char data){ //Muestra data en formato decimal
	unsigned char divisor=100;
	while(divisor){
		print((char)(data/divisor+0x30));
		data%=divisor;
		divisor/=10;
	}
	return 3;
}

unsigned int mPrint:: printUIntHex(unsigned int data){ //Muestra data en formato hexadecimal, NO agrega "Ox"
	unsigned int divisor=4096;
	unsigned char aux;
		do {
			aux=data/divisor;
			aux=aux>9?aux+55:aux+0x30;
			print((char)(aux));
			data%=divisor;
			divisor/=16;
		}while(divisor);
		return 4;
}

unsigned int mPrint:: printUCharHex(unsigned char data){ //Muestra data en formato hexadecimal, NO agrega "Ox"
	unsigned char divisor=16;
	unsigned char aux;
		do {
			aux=data/divisor;
			aux=aux>9?aux+55:aux+0x30;
			print((char)(aux));
			data%=divisor;
			divisor/=16;
		}while(divisor);
		return 2;
}

