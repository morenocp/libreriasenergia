#ifndef mPrint_h
#define mPrint_h

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2

#include <string.h>

class mPrint
{
  private:
    int write_error;
    unsigned int printNumber(unsigned long, unsigned char);
    unsigned int printFloat(double, unsigned char);
  protected:
    void setWriteError(int err = 1) { write_error = err; }
  public:
    mPrint() : write_error(0) {};
  
    int getWriteError() { return write_error; }
    void clearWriteError() { setWriteError(0); }
  
    virtual unsigned int write(unsigned char) = 0;
    unsigned int write(const char *str) { return write((const unsigned char *)str, strlen(str)); }
    virtual unsigned int write(const unsigned char *buffer, unsigned int size);

    unsigned int print(const char[]);
    unsigned int print(char);
    unsigned int print(unsigned char, int = DEC);
    unsigned int print(int, int = DEC);
    unsigned int print(unsigned int, int = DEC);
    unsigned int print(long, int = DEC);
    unsigned int print(unsigned long, int = DEC);
    unsigned int print(double, int = 2);
    unsigned int println(const char[]);
    unsigned int println(char);
    unsigned int println(unsigned char, int = DEC);
    unsigned int println(int, int = DEC);
    unsigned int println(unsigned int, int = DEC);
    unsigned int println(long, int = DEC);
    unsigned int println(unsigned long, int = DEC);
    unsigned int println(double, int = 2);
    unsigned int println(void);
    //funciones agregadas para compatibilidad con mi  libreria de lcd
    unsigned int printUInt(unsigned int data);
    unsigned int printUIntDP(unsigned int data,unsigned char dp);
    unsigned int printUChar(unsigned char data);
    unsigned int printUIntHex(unsigned int data);
    unsigned int printUCharHex(unsigned char data);
};

#endif
