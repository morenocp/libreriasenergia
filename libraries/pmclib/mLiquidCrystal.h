/*
 * LiquidCrystal.h
 *
 *  Created on: 04/11/2014
 *      Author: pedro
 */

#ifndef LIQUIDCRYSTAL_H_
#define LIQUIDCRYSTAL_H_
#include "mPrint.h"


// commands
#define LCD_CLEARDISPLAY 0x01 				//Comando: Borra pantalla
#define LCD_RETURNHOME 0x02					//Comando: Cursor a direccion 0
#define LCD_ENTRYMODESET 0x04				//Comando: Modo entrada
#define LCD_DISPLAYCONTROL 0x08				//Comando: Control de pantalla
#define LCD_CURSORSHIFT 0x10				//Comando: Control de los desplazamientos del cursor y de la pantalla
#define LCD_FUNCTIONSET 0x20				//Comando: Configuracion del hardware
#define LCD_SETCGRAMADDR 0x40				//Comando: Modificar el apuntador a la DDRAM
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x02					//Incremento automatico de la posicion del cursor
#define LCD_ENTRYLEFT 0x00					//Decremento automatico de la posicion del cursor
#define LCD_ENTRYSHIFTINCREMENT 0x01		//Desplazamiento de la pantalla al escribir un nuevo caracter
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04					//Display encendido
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02					//Cursor visualizado
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01					//Cursor parpadea
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08				//El desplazamiento se aplica sobre todo el display
#define LCD_CURSORMOVE 0x00					//El desplazamiento se aplica sobre el cursor
#define LCD_MOVERIGHT 0x04					//Desplazamiento a la derecha
#define LCD_MOVELEFT 0x00					//Desplazamiento a la izquierda

// flags for function set
#define LCD_8BITMODE 0x10					//Modo 8 bits
#define LCD_4BITMODE 0x00					//Modo 4 bits
#define LCD_2LINE 0x08						//2 linea
#define LCD_1LINE 0x00						//1 linea
#define LCD_5x10DOTS 0x04					// Fuente 5x10
#define LCD_5x7DOTS 0x00					// Fuente 5x7

class LiquidCrystal : public mPrint {
public:
	LiquidCrystal(){};
	~LiquidCrystal(){};

  void clear();
  void home();
  void noDisplay();
  void display();
  void noBlink();
  void blink();
  void noCursor();
  void cursor();
  void scrollDisplayLeft();
  void scrollDisplayRight();
  void leftToRight();
  void rightToLeft();
  void autoscroll();
  void noAutoscroll();
  void createChar(unsigned char, unsigned char[]);
  void setCursor(unsigned char, unsigned char);
  void setDisplayfunction(unsigned char displayfunction);
  void setDisplayControl(unsigned char displaycontrol);
  void setDisplayMode(unsigned char displaymode);

  virtual unsigned int write(unsigned char)=0;
  virtual unsigned int command(unsigned char)=0;
private:
  unsigned char _displayfunction;
  unsigned char _displaycontrol;
  unsigned char _displaymode;

};

#endif /* LIQUIDCRYSTAL_H_ */
