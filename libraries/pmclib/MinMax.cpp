/*
 * MinMax.cpp
 *
 *  Created on: 1 ago. 2019
 *      Author: pedro
 */

#include <MinMax.h>

MinMax::MinMax(float limMin,float limMax)
{
    limitMax=limMax;
    limitMin=limMin;
    reset();
}

float MinMax::getMin(){
    return minimum;
}

float MinMax::getMax(){
    return maximum;
}


float MinMax::getDelta(){
    return maximum-minimum;
}

float MinMax::getAverage(){
    return (maximum+minimum)/2;
}

void MinMax::update(float value){
    maximum=value>maximum?value:maximum;
    minimum=value<minimum?value:minimum;
}

void MinMax::reset(){
    minimum=limitMax;
    maximum=limitMin;
}

void MinMax::reset(float limMin,float limMax){
    minimum=limMax;
    maximum=limMin;
}

