/*
 * pwmTimer1.h
 *
 *  Created on: 24/03/2017
 *      Author: pedro
 */

#ifndef PWMTIMER1_H_
#define PWMTIMER1_H_

#include <msp430.h>
#include <bitman.h>

#define pwmPort P2OUT
#define pwmPortDir P2DIR
#define pwmPortSel P2SEL
#define _pwmCW BIT1




void pwmSetDuty(unsigned int duty){
    TA1CCR1=duty;
}

void pwmSetup(void){
    BIS(pwmPortDir,_pwmCW);
    BIS(pwmPortSel,_pwmCW);
    BIS(TA1CTL,TASSEL_2); //TimerA1 con fuente SMCLK
    TA1CCR0=256; //Frecuencia PWM=16MHz/256=62.5 kHz
    pwmSetDuty(128); //Ciclo de trabajo =50%;
    BIS(TA1CCTL1,OUTMOD_7); //TimerA1 Reg1 en modo PWM ResetSet
    BIS(TA1CTL,MC_1); //Se pone en funcionamiento el Timer A1 en modo up

}

void mdcRun(void){
    BIS(TA1CTL,MC_1);
}



#endif /* PWMTIMER1_H_ */
