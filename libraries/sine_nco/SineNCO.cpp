/*
 * SineNCO.cpp
 *
 *  Created on: 12 ene. 2017
 *      Author: pedro
 */

#include "SineNCO.h"

SineNCO::SineNCO(){
    accumulator.phase=0; increment=0;
}

unsigned char SineNCO::getPhase(){
    return accumulator.phaseByte[2]&0x7f;
}
SineNCO::~SineNCO(){}

void SineNCO::step(){
    accumulator.phase+=increment;
}

unsigned char SineNCO::analogOut(){
    return sineTable(getPhase());
}

unsigned char SineNCO::digitalOut(){
    return getPhase()<64?0xff:0x00;
}

void SineNCO::setIncrement(unsigned long new_inc){
    increment=new_inc;
}

void SineNCO::clear(){
    accumulator.phase=0;
}


unsigned char SineNCO::sineTable(unsigned char ph){
    static const unsigned char sinTable[]={
                  128,134,140,146,152,158,163,169,175,180,186,191,196,201,205,210,214,
                  218,222,226,229,233,236,238,241,243,245,246,248,249,249,250,250,
                  250,249,249,248,246,245,243,241,238,236,233,229,226,222,218,214,
                  210,205,201,196,191,186,180,175,169,163,158,152,146,140,134,128,
                  122,116,110,104,98,93,87,81,76,70,65,60,55,51,46,42,
                  38,34,30,27,23,20,18,15,13,11,10,8,7,7,6,6,
                  6,7,7,8,10,11,13,15,18,20,23,27,30,34,38,42,
                  46,51,55,60,65,70,76,81,87,93,98,104,110,116,122};
          return sinTable[ph];
}
