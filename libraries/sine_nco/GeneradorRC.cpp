/*
 * GeneradorRC.cpp
 *
 *  Created on: 19 ene. 2017
 *      Author: pedro
 */

#include "GeneradorRC.h"

GeneradorRC::GeneradorRC():SineNCO()
{
    On=false;
    damping=false;
    trigger=0;
    isRunning=false;
    evaluated=false;

}

void GeneradorRC::evaluateTrigger()
{
    unsigned char trig=damping?0xff:digitalOut();
    trigger=(!On)?0:trig;
}

void GeneradorRC::step(){
    if(!evaluated){
        if(isRunning){
            SineNCO::step();
            switch(getPhase()){
                case 0:
                    if(!On) isRunning=false;
                case 64:
                    evaluateTrigger();
                    break;
            }
        }
        evaluated=true;
    }
}

void GeneradorRC::setOn(bool value){
    On=value;
    if(!isRunning) isRunning=true;
}

unsigned char GeneradorRC::analogOut(){
    evaluated=false;
    return SineNCO::analogOut();
}
