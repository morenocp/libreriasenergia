/*
 * SineNCO.h
 *
 *  Created on: 12 ene. 2017
 *      Author: pedro
 */

#ifndef SINENCO_H_
#define SINENCO_H_

union PhaseType {
        unsigned long phase;
        unsigned char phaseByte[4];
    };

class SineNCO
{


        PhaseType accumulator;
        unsigned long increment;
        static unsigned char sineTable(unsigned char ph);
    public:
        SineNCO();
        ~SineNCO();
        unsigned char getPhase();
        void step();
        unsigned char analogOut();
        unsigned char digitalOut();
        void setIncrement(unsigned long new_inc);
        void clear();

};
#endif /* SINENCO_H_ */
