/*
 * GeneradorRC.h
 *
 *  Created on: 19 ene. 2017
 *      Author: pedro
 */

#ifndef GENERADORRC_H_
#define GENERADORRC_H_

#include "SineNCO.h"

class GeneradorRC: public SineNCO
{
    bool On;
    bool damping;
    bool isRunning;
    unsigned char trigger;
    bool evaluated;
protected:
    void evaluateTrigger();

public:
    GeneradorRC();
    virtual ~GeneradorRC(){};
    void setDamping(bool value){damping=value;}
    void setOn(bool value);
    unsigned char getTrigger(){return trigger;};
    void step();
    unsigned char analogOut();
};

#endif /* GENERADORRC_H_ */
