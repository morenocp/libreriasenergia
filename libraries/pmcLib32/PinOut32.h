/*
 * PinOut.h
 *
 *  Created on: 20 feb. 2018
 *      Author: pedro
 */

#ifndef PINOUT_H_
#define PINOUT_H_

#include <stdint.h>


class PinOut32
{
private:
   uint8_t mask;
   uint8_t port;

public:

     PinOut32(uint8_t selectedPort, uint8_t selectedPins,uint8_t initvalue=0);
     void set();
     void clear();
     void set(bool value);
     void toggle();
};

#endif /* PINOUT_H_ */
