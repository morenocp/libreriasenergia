/*
 * HX711_32.h
 *
 *  Created on: 20 feb. 2018
 *      Author: pedro
 */

#ifndef HX711_32_H_
#define HX711_32_H_
#include <PinOut32.h>
#include <PinIn32.h>

class HX711_32
{
private:
        PinOut32* PD_SCK;    // Power Down and Serial Clock Input Pin
        PinIn32* DOUT;      // Serial Data Output Pin
        unsigned char GAIN;      // amplification factor
        long OFFSET;    // used for tare weight
        float SCALE;    // used to return weight in grams, kg, ounces, whatever
        unsigned char shiftIn();
        void setSCK();
        void clearSCK();
        bool isClearDTA();
        bool isSetDTA();

    public:
        // define clock and data pin, channel, and gain factor
        // channel selection is made by passing the appropriate gain: 128 or 64 for channel A, 32 for channel B
        // gain: 128 or 64 for channel A; channel B works with 32 gain factor only
        HX711_32(PinIn32& dout, PinOut32& pd_sck);

        virtual ~HX711_32();

        // Allows to set the pins and gain later than in the constructor
        void begin(unsigned char gain = 128);

        // check if HX711_32 is ready
        // from the datasheet: When output data is not ready for retrieval, digital output pin DOUT is high. Serial clock
        // input PD_SCK should be low. When DOUT goes to low, it indicates data is ready for retrieval.
        bool isReady();

        // set the gain factor; takes effect only after a call to read()
        // channel A can be set for a 128 or 64 gain; channel B has a fixed 32 gain
        // depending on the parameter, the channel is also set to either A or B
        void setGain(unsigned char gain = 128);

        // waits for the chip to be ready and returns a reading
        long read();

        // returns an average reading; times = how many times to read
        long readAverage(unsigned char times = 10);

        // returns (read_average() - OFFSET), that is the current value without the tare weight; times = how many readings to do
        double getValue(unsigned char times = 1);

        // returns get_value() divided by SCALE, that is the raw value divided by a value obtained via calibration
        // times = how many readings to do
        float getUnits(unsigned char times = 1);

        // set the OFFSET value for tare weight; times = how many times to read the tare value
        void tare(unsigned char times = 10);

        // set the SCALE value; this value is used to convert the raw data to "human readable" data (measure units)
        void setScale(float scale = 1.f);

        // get the current SCALE
        float getScale();

        // set OFFSET, the value that's subtracted from the actual reading (tare weight)
        void setOffset(long offset = 0);

        // get the current OFFSET
        long getOffset();

        // puts the chip into power down mode
        void powerDown();

        // wakes up the chip after power down mode
        void powerUp();
};

#endif /* HX711_32_H_ */
