/*
 * PinIn32.h
 *
 *  Created on: 20 feb. 2018
 *      Author: pedro
 */

#ifndef PinIn32_H_
#define PinIn32_H_
#include <stdint.h>




class PinIn32
{
private:
   uint8_t mask;
   uint8_t port;
public:
   enum INPUT_TYPE{INPUT,INPUT_PULLUP,INPUT_PULLDOWN};
     PinIn32(uint8_t selectedPort, uint8_t selectedPins,PinIn32::INPUT_TYPE mode=INPUT);
     bool isSet();
     bool isClear();
     void enableInt();
     void disableInt();
     void clearInt();

};

#endif /* PinIn32_H_ */
