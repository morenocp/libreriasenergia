/*
 * HX711_32.cpp
 *
 *  Created on: 20 feb. 2018
 *      Author: pedro
 */

#include <HX711_32.h>

HX711_32::HX711_32(PinIn32& dout, PinOut32& pd_sck) {
    DOUT=&dout;
    PD_SCK=&pd_sck;
    OFFSET=0;
    SCALE=1;
}


HX711_32::~HX711_32() {
}

void HX711_32::begin(unsigned char gain) {
    setGain(gain);
}

bool HX711_32::isReady() {
    return isClearDTA(); // digitalRead(DOUT) == LOW;
}

void HX711_32::setGain(unsigned char gain) {
    switch (gain) {
        case 128:       // channel A, gain factor 128
            GAIN = 1;
            break;
        case 64:        // channel A, gain factor 64
            GAIN = 3;
            break;
        case 32:        // channel B, gain factor 32
            GAIN = 2;
            break;
    }

    clearSCK(); //digitalWrite(PD_SCK, LOW);
    read();
}

long HX711_32::read() {
    // wait for the chip to become ready
    while (!isReady()) {


    }

    unsigned long value = 0;
    unsigned char data[3] = { 0 };
    unsigned char filler = 0x00;

    // pulse the clock pin 24 times to read the data
    data[2] = shiftIn();
    data[1] = shiftIn();
    data[0] = shiftIn();

    // set the channel and the gain factor for the next reading using the clock pin
    for (unsigned int i = 0; i < GAIN; i++) {
        setSCK(); //digitalWrite(PD_SCK, HIGH);
        clearSCK();//digitalWrite(PD_SCK, LOW);
    }

    // Replicate the most significant bit to pad out a 32-bit signed integer
    if (data[2] & 0x80) {
        filler = 0xFF;
    } else {
        filler = 0x00;
    }

    // Construct a 32-bit signed integer
    value = ( static_cast<unsigned long>(filler) << 24
            | static_cast<unsigned long>(data[2]) << 16
            | static_cast<unsigned long>(data[1]) << 8
            | static_cast<unsigned long>(data[0]) );

    return static_cast<long>(value);
}

long HX711_32::readAverage(unsigned char times) {
    long sum = 0;
    for (unsigned char i = 0; i < times; i++) {
        sum += read();

    }
    return sum / times;
}

double HX711_32::getValue(unsigned char times) {
    return readAverage(times) - OFFSET;
}

float HX711_32::getUnits(unsigned char times) {
    return getValue(times) / SCALE;
}

void HX711_32::tare(unsigned char times) {
    double sum = readAverage(times);
    setOffset(sum);
}

void HX711_32::setScale(float scale) {
    SCALE = scale;
}

float HX711_32::getScale() {
    return SCALE;
}

void HX711_32::setOffset(long offset) {
    OFFSET = offset;
}

long HX711_32::getOffset() {
    return OFFSET;
}

void HX711_32::powerDown() {
    clearSCK();//digitalWrite(PD_SCK, LOW);
    setSCK(); //digitalWrite(PD_SCK, HIGH);
}

void HX711_32::powerUp() {
    clearSCK(); //digitalWrite(PD_SCK, LOW);
}

unsigned char HX711_32::shiftIn() {
    unsigned char value = 0;
    unsigned char i;

    for (i = 0; i < 8; ++i) {
        setSCK();                      //digitalWrite(clockPin, HIGH);
        value |= isSetDTA()<<(7-i);  //digitalRead(dataPin) << (7 - i);
        clearSCK();                    //digitalWrite(clockPin, LOW);
    }
    return value;
}
 void HX711_32::setSCK(){
     PD_SCK->set();
 }

 void HX711_32::clearSCK(){
     PD_SCK->clear();
 }

 bool HX711_32::isClearDTA(){
     return DOUT->isClear();

 }
 bool HX711_32::isSetDTA(){
     return DOUT->isSet();
 }
