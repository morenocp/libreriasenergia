/*
 * PinIn32.cpp
 *
 *  Created on: 20 feb. 2018
 *      Author: pedro
 */

#include <PinIn32.h>
//#include <ti/devices/msp432p4xx/driverlib/gpio.h>
#include <gpio.h>

PinIn32::PinIn32(uint8_t selectedPort, uint8_t selectedPins,PinIn32::INPUT_TYPE mode)
{
   port=selectedPort;
   mask=selectedPins;
   switch(mode){
           case INPUT_PULLUP:
               GPIO_setAsInputPinWithPullUpResistor(port,mask);
               break;
           case INPUT_PULLDOWN:
               GPIO_setAsInputPinWithPullDownResistor(port,mask);
               break;
           default:
               GPIO_setAsInputPin(port,mask);
               break;
   }
}


bool PinIn32::isSet(){
   return (GPIO_getInputPinValue(port,mask)!=0);
}

bool PinIn32:: isClear(){
   return not isSet();
}

void PinIn32::enableInt(){
    GPIO_enableInterrupt(port,mask);
}
void PinIn32::disableInt(){
    GPIO_disableInterrupt(port,mask);
}
void PinIn32::clearInt(){
    GPIO_clearInterruptFlag (port,mask);
}
