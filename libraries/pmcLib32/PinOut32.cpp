/*
 * PinOut32.cpp
 *
 *  Created on: 20 feb. 2018
 *      Author: pedro
 */

#include <PinOut32.h>
//#include <ti/devices/msp432p4xx/driverlib/gpio.h>
#include <gpio.h>


PinOut32::PinOut32(uint8_t selectedPort, uint8_t selectedPins,uint8_t initvalue)
{
    port=selectedPort;
    mask=selectedPins;
    set(initvalue);
    GPIO_setAsOutputPin (port,mask);
}

void PinOut32::set(){
    GPIO_setOutputHighOnPin(port,mask);
}
void PinOut32::clear(){
    GPIO_setOutputLowOnPin(port,mask);
}
void PinOut32::set(bool value){
    if(value) set(); else clear();
}
void PinOut32::toggle(){
    GPIO_toggleOutputOnPin(port,mask);
}



