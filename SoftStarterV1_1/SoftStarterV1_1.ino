
#include "globals.h"
#include "prototypes.h"



void setup() {
  pwm1.init(PERIODE_PWM,TASSEL_2 | ID_3 | MC_1,OUTMOD_3); //Se configuran los 2 modulos PWM
  pwm2.init(16500,TASSEL_2 | ID_3 | MC_1,OUTMOD_7);
  Serial.begin(115200);
  Serial.println("Setup system...");
  setupCallback();
  pwm1.clear();
  pwm2.clear();
  pwm2.setDuty(PERIODE_PWM/10);
  pinPWM2.setDigitalMode(false);


  // Se limpian las banderas de interrupcion, se configuran los flancos de int y se activa la int del switch
 line1.clearIntFlag();
 boton.clearIntFlag();
 line1.setIntEdge(true);  //Flanco de bajada
 boton.setIntEdge(true);
 boton.enableInt(true);
 Serial.println("System ready");
}

void loop() {
  
  ledIsRun.set(line1.isEnableInt());  //Led indicando cuano el algoritmo de arranque/detencion esta operando
  if(Serial.available()) firmata.parse(Serial.read());  //Se procesan los datos provenientes de la computadora
  
  if(startUp.isSet()){        //Se arranca el motor
    
    line1SF.setPeriodePWM(tPWM); //Se establecen los parametros de arranque
    line1SF.setRampTime(rampTime);
    delay(300);
    pinPWM1.setDigitalMode(false); //Se habilita la funcion de PWM en el pin correspondiente
    bool isButtonPress=true; //boton.isClear(); 
    line1SF.start(isButtonPress);  //Se inicia el algoritmo de arranque
    boton.setIntEdge(!isButtonPress); //Se configura el boton para interrumpir en el flanco de subida
    line1.enableInt(true);            //Se habilita la interrupcion por la  linea 1
  }
  if(startDown.isSet()){      //Se detiene el motor
    pinPWM1.setDigitalMode(true);  //Se activa el modo digital del pin
    line1SF.start(false);           //Se inicia el proceso de detencion del motor
    line1.enableInt(true);          //Se habilita la interrupcion por la  linea 1
  }
  
 if (sendInfo.isZero()){            //Cada determinado tiempo se envia informacion a la computadora.
    Serial.print("rt[s]= ");
    Serial.print(rampTime,3);
    Serial.print(" angle=");   
    Serial.println(tPWM*180.0/PERIODE_PWM,0);  
  }
}


//ISR
  #pragma vector= PORT1_VECTOR //vector de interrupcion del puerto1
  __interrupt void isr_port1(void) { //rutina de servicio
  ISR_CERO();
  }



  void ISR_CERO() {
  uint8_t reg = P1IFG;
  
  if (boton.isIntFlag()) {  
      //start.set();
      boton.clearIntFlag();
      
  }
  if (line1.isIntFlag()) {  //Interrupcion por el cruce por cero de la linea 1
    if (line1SF.next()) {   //Si todavia sigue operando el algoritmo
      pwm1.clear();         //reiniciar la cuenta del PWM
      pwm1.setDuty(line1SF.getValue()); //Establecer el nuevo valor del ciclo de trabajo
    } else {
      line1.enableInt(false);           //Deshabilitar la generacion de int de la linea 1
     // enablePWM(false,pinPWM1);
    }
    relay1.set(line1SF.getRelayState());
    line1.clearIntFlag();
  }

  
}

void adcReadParameters(){
  rampTime = MAX_RAMPTIME*analogRead(P1_6)/1023+MIN_RAMPTIME;
  tPWM = MAX_TPWM*analogRead(P1_7)/1023+MIN_TPWM;
}

void setBoolData(void * context, uint8_t channel, uint16_t value){
  if(channel==3){
      if(value==1) {
        startUp.set();
        //Serial.println("Arrancando...");
      } else
      {
        startDown.set();
       // debugPrint("Detenido");
      }
  }
}

void setInt14Data(void * context, uint8_t channel, uint16_t value){
  switch(channel){
    case 2:
      tPWM=value;
      break;
    case 1:
    rampTime=(float)value/1000.0;

     break;   
  }
}


void setupCallback(){
  firmata.attach(ANALOG_MESSAGE,setInt14Data,0);
  firmata.attach(SET_DIGITAL_PIN_VALUE,setBoolData,0);
  
}
