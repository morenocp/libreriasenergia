#ifndef GLOBALS_H
#define GLOBALS_H


#include "SoftStarter.h"
#include "CFlag.h"
#include "DigitalPinOut.h"
#include "DigitalPinIn.h"
#include "PMCFirmataConstants.h"
#include "PMCFirmataParser.h"
#include "PWM2553.h"
#include "Countdown.h"
#include "msp430.h"




#define PERIODE_PWM 16042
#define MAX_RAMPTIME 15.0
#define MIN_RAMPTIME 0.1
#define MAX_TPWM PERIODE_PWM
#define MIN_TPWM 10




//Variables comunes
unsigned char firmatabuffer[32];
FirmataParser firmata(firmatabuffer,sizeof(firmatabuffer));
CFlag startUp,startDown;                  //Banderas para arrancar o detener 
DigitalPinIn boton(Port1,BIT3);           //Switch de encendido y apagado manual
DigitalPinOut ledIsRun(Port1, BIT6);      //Led para indicar cuando el sistema esta en ejecucion
float rampTime=MIN_RAMPTIME;              //Tiempo de arranque
uint16_t tPWM=PERIODE_PWM;                //Valor inicial cuando se arranca el motor. 
Countdown sendInfo(64000);                //Contador para enviar info al host

//Para linea1
SoftStarter line1SF(PERIODE_PWM+3);       //Arrancador suave par la linea 1
DigitalPinOut relay1(Port2, BIT5);        //Relevador para la linea 1
DigitalPinOut pinPWM1(Port2, BIT1);       //Pin de salida de la señal PWM para la linea 1
DigitalPinIn line1(Port1,BIT5);           // Linea 1 de entrada, viene del detector de cruce por cero de la linea 1
PWM2553 pwm1(TimerA1,1);                  //Modulo PWM para la linea 1, Timer A1, registro 1

//Para linea2
SoftStarter line2SF(PERIODE_PWM+3);       //Arrancador suave par la linea 2
DigitalPinOut relay2(Port2, BIT4);        //Relevador para la linea 2
DigitalPinOut pinPWM2(Port2, BIT6);       //Pin de salida de la señal PWM para la linea 2
DigitalPinIn line2(Port1,BIT4);           // Linea 2 de entrada, viene del detector de cruce por cero de la linea 2
PWM2553 pwm2(TimerA0,1);                  //Modulo PWM para la linea 2, Timer A0, registro 1

#endif
