#ifndef PROTOTYPES_H
#define PROTOTYPES_H

#include "stdint.h"

//Funcion callback para valores booleanos provenientes del protocolo firmata, se usa para encender o arrancar el motor
void setBoolData(void * context, uint8_t channel, uint16_t value);

//Funcion callback para valores booleanos provenientes del protocolo firmata, se usa para establecer los valores de tPWM, y tiempo de arranque
void setInt14Data(void * context, uint8_t channel, uint16_t value);

//Funcion para configurar las funciones callbak del protocolo firmata
void setupCallback();

//Rutina de atencion a interrupcion de las lineas de AC y del switch de encendido y apagado
void ISR_CERO();

//Lee los parametros del arrancador de los potenciometros.
void adcReadParameters();


#endif
