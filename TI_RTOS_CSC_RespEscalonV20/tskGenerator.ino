#include "globals.h"

#define IsValueX true
#define IsValueY false


void updateEqH();


void setup3() {    
  generh.setOffset(0);                    
  generh.setIntervalValues(6,15,false);
  dpidh.preValue=0;

}


void loop3() {
 
 float valSen=0;
 bool asc;

  if (runGenerator.isSet() && generh.getEnable()) {

      valSen=generh.getValue(false);
      asc=generh.isSlopeUp();;
      dpidh.output=mapH.map(valSen,asc);
      setOutputPWM((uint16_t)dpidh.output);
      updateEqH();
      if (generh.next()) {
        dpidh.delayAutomatic=20;
      }
    }
  
}

void setDefaultGainH(float x, float y,float gain){
  float offset=y-gain*x;
  mapH.setSlope(gain,true);
  mapH.setOffset(offset,true);
  mapH.setSlope(gain,false);
  mapH.setOffset(offset,false);
}


void updateEqH(){
   unsigned int phase=generh.getPhase();
   unsigned int cycle=generh.getCycleNumber();
   float minF,maxF,minP,maxP,deltaF;
   limitesH.update(sensorsValue[canalFuerzaH]);
   limitesPWMH.update(dpidh.output);
   if(cycle==1  && phase==9) {
    limitesH.reset();
    limitesPWMH.reset();
   }
   if(phase==0 && cycle>1){
        minF=limitesH.getMin();
        maxF=limitesH.getMax();
        minP=limitesPWMH.getMin();
        maxP=limitesPWMH.getMax();
        deltaF=limitesH.getDelta();
        if(deltaF==0) setDefaultGainH(maxF,maxP,gainMaxH);
        else
        {
          deltaF=0.5*((limitesPWMH.getDelta()+mapH.getSlope(true)*2*generh.getAmplitud())/deltaF);
          deltaF=deltaF>gainMaxH?gainMaxH:deltaF;
          setDefaultGainH(maxF,maxP,deltaF);
          /*
          mapH.setPoint2D(0,minF,minP);
          mapH.setPoint2D(1,maxF,maxP);
          mapH.setPoint2D(2,minF,minP);
          mapH.setPoint2D(3,maxF,maxP);
          mapH.updateEqs();
          if (mapH.getSlope(true)>gainMaxH) setDefaultGainH(maxF,maxP,gainMaxH);
          printSerial1(minF,minP,maxF,maxP);*/
        
        }  
         printSerial1(mapH.getSlope(true),mapH.getOffset(true));
         limitesH.reset();
         limitesPWMH.reset();
   }
}


void setDefaultSlopeH(){
  mapH.setSlope(CAL_SLOPE_ASC,true);
  mapH.setSlope(CAL_SLOPE_DES,false);
  
}


//Este metodo funciona parcialmente, produce un sobrepico de 2*amplitud en el tercer ciclo.
/*void updateEqH(){
   unsigned int phase=generh.getPhase();
   unsigned int cycle=generh.getCycleNumber();
   float amplitud,pwmcero;
   limitesH.update(sensorsValue[canalFuerzaH]);
   if(cycle==1  && phase==9) limitesH.reset();
   if(phase==0 && cycle>1){
         pwmcero=2*mapH.getOffset(true)-mapH.map(limitesH.getAverage(),true);
         amplitud=2*mapH.getSlope(true)*generh.getAmplitud()/limitesH.getDelta();
         mapH.setOffset(pwmcero,true);
         mapH.setSlope(amplitud,true);
         limitesH.reset();
   }
}*/
