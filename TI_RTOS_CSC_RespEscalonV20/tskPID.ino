#include "globals.h"
#include "msp.h"
#include "bitman.h"


#define pinPWM BIT5
#define pinPWM_RIGHT BIT5
#define pinPWM_LEFT BIT6
#define PWM_PERIODE 4095



void clockFunctionPID()
{

  runPID.set();
  if(dpidh.delayAutomatic && ((--dpidh.delayAutomatic)==0))  pidh.SetMode(AUTOMATIC);
}

void setOutputPWM(uint16_t value) {
  TIMER_A0->CCR[2] = value;
}





void setup4() {
  //Configuracion del PWM
  P2->OUT&=~(pinPWM_RIGHT+pinPWM_LEFT);
  P2->DIR |= pinPWM_RIGHT+pinPWM_LEFT; //funcion PWM
  BIS(P2->SEL0,pinPWM_RIGHT+pinPWM_LEFT);
  TIMER_A0->CCR[0] = PWM_PERIODE;
  TIMER_A0->CCR[2] = 0;
  TIMER_A0->CCTL[2] = TIMER_A_CCTLN_OUTMOD_7;
  TIMER_A0->CCR[3] = 0;
  TIMER_A0->CCTL[3] = TIMER_A_CCTLN_OUTMOD_7;
  TIMER_A0->CTL = TIMER_A_CTL_TASSEL_2 | TIMER_A_CTL_MC_1 | TIMER_A_CTL_CLR| TIMER_A_CTL_ID_0 ;//fuente SMCLK/4,/ periodo 65535,Fpwm=128 Hz
  //Configuracion del PID    
  dpidv.sp = 0; 
  dpidv.output=0;
  pidh.SetMode(MANUAL);
  pidh.SetSampleTime(HDEFAULT_PID_SAMPLETIME);
  pidh.SetTunings(dpidh.kp, dpidh.ki, dpidh.kd);
  pidh.SetOutputLimits(0, (PWM_PERIODE-1));
  clkPID.begin(clockFunctionPID, 150, HDEFAULT_PID_SAMPLETIME);
  clkPID.start();
}

void loop4() {

 if (runPID.isSet()) {
    dpidh.input=sensorsValue[canalFuerzaH];
    pidh.Compute();
    setOutputPWM((uint16_t) dpidh.output);
  }
 
}
