#include "globals.h"

CFlag sendData;

void clockFunctionSampler()
{

  sendData.set();
  runGenerator.set();
  runGeneratorV.set();
  wtdi2c--;
}


void setup2() {
  Serial.begin(115200);
  miBuffer.clear();
  clkSampler.begin(clockFunctionSampler, 1000,100);
  clkSampler.start();
}

void sendRespStep(){
  Serial.print("P:");
  Serial.print(millis()/1000.0,1);
    Serial.print(";V:");
    Serial.print(sensorsValue[canalFuerzaV], 1);
    Serial.print(";H:");
    Serial.print(sensorsValue[canalFuerzaH], 1);
    Serial.print(";D:");
    Serial.print(dpidv.output,0);
    Serial.print(";L:");
    Serial.print(dpidh.output,0);
    Serial.println(";");
}

void sendJSONValues(){
  Serial.print("{\"S\":[");
  Serial.print(sensorsValue[canalPresion], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalFuerzaV], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalFuerzaH], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalLVDTV], 3);
  Serial.print(",");
  Serial.print(sensorsValue[canalLVDTH], 3);
  Serial.print(",");
  Serial.print(sensorsValue[canalVolumen], 2);
  Serial.print(",");
  Serial.print(sensorsValue[canalContrapresion], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalConfinante], 1);
  Serial.print("]}\n");
}

void sendJSONRespStep(){
  Serial.print("{\"S\":[");
  Serial.print(sensorsValue[canalPresion], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalFuerzaV], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalFuerzaH], 1);
  Serial.print(",");
  Serial.print(dpidv.output,0);
  Serial.print(",");
  Serial.print(dpidh.output,0);
  Serial.print(",");
  Serial.print(sensorsValue[canalVolumen], 2);
  Serial.print(",");
  Serial.print(sensorsValue[canalContrapresion], 1);
  Serial.print(",");
  Serial.print(sensorsValue[canalConfinante], 1);
  Serial.print("]}\n");
}




void sendValues(){
  Serial.print("P:");
    Serial.print(sensorsValue[canalPresion], 1);
    Serial.print(";V:");
    Serial.print(sensorsValue[canalFuerzaV], 1);
    Serial.print(";H:");
    Serial.print(sensorsValue[canalFuerzaH], 1);
    Serial.print(";D:");
    Serial.print(sensorsValue[canalLVDTV], 3);
    Serial.print(";L:");
    Serial.print(sensorsValue[canalLVDTH], 3);
   // Serial.print(";W:");
   // Serial.print(dpidh.output,0);
    Serial.println(";");
}

void loop2() {
  char ch;
  if (wtdi2c==0) resetCommI2C();
  
  if (sendData.isSet()) {
      if(sendPWMData) 
      sendJSONRespStep();  
      else 
      sendJSONValues();  
  }
 if(Serial.available()) {
  ch=Serial.read();
  miBuffer.push(ch);
 }
}
