#include "globals.h"
#include "ADS1115.h"
#include <Wire.h>
#define I2C_LVDTV 0x48
#define I2C_LVDTH  0x49

#define S4_SCALE 5703.5800957003  //Constantes de calibración LVDT Vertical
#define S4_OFFSET -28519
#define S5_SCALE 3379.8119918352  //Constantes de calibracion LVDT Horizontal
#define S5_OFFSET -1827 //=cero a media carrera,-31323=cero completamente extendido


ADS1115 LVDTV(I2C_LVDTV);
ADS1115 LVDTH(I2C_LVDTH);


void clockFunctionI2CSampler()
{
 
  sampleI2C.set();
}

void resetCommI2C(){
  LVDTH.begin();
  wtdi2c=20;
}

void setup6() {
  sensores[3].setScale(S4_SCALE);
  sensores[4].setScale(S5_SCALE);
  sensores[3].setOffset(S4_OFFSET);
  sensores[4].setOffset(S5_OFFSET);

  LVDTH.begin();
  uint16_t configV = ADS1115_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                    ADS1115_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
                    ADS1115_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                    ADS1115_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                    ADS1115_CONVERSIONDELAY_250SPS   | // 250 samples per second (default)
                    ADS1115_REG_CONFIG_MUX_DIFF_0_1 | //differential mode, chael 0-1
                    ADS1115_REG_CONFIG_PGA_1_024V |
                    ADS1115_REG_CONFIG_MODE_CONTIN;   // Continuos mode (default)
  uint16_t configH = ADS1115_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                    ADS1115_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
                    ADS1115_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                    ADS1115_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                    ADS1115_CONVERSIONDELAY_250SPS   | // 250 samples per second (default)
                    ADS1115_REG_CONFIG_MUX_DIFF_0_1 | //differential mode, chael 0-1
                    ADS1115_REG_CONFIG_PGA_2_048V |
                    ADS1115_REG_CONFIG_MODE_CONTIN;   // Continuos mode (defaul
 LVDTV.writeRegister(I2C_LVDTV, ADS1115_REG_POINTER_CONFIG, configV);
 LVDTH.writeRegister(I2C_LVDTH, ADS1115_REG_POINTER_CONFIG, configH);
 clkI2C.begin(clockFunctionI2CSampler, 2900, 100);
 clkI2C.start();
}



void loop6() {
  char ch;
  if (sampleI2C.isSet()) {
    sensores[3].setValueFromLong((long)LVDTV.getLastConversionResults());
    sensores[4].setValueFromLong((long)LVDTH.getLastConversionResults());
    sensorsValue[3]=sensores[3].getUnits();
    sensorsValue[4]=sensores[4].getUnits();
    wtdi2c=20;
}
}
