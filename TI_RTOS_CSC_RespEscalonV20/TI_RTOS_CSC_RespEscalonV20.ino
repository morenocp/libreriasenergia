#include "globals.h"

//Constants Sensors
//#define S1_SCALE -6025.678571  //Constantes transductor presion
#define S1_SCALE -6417.806551
//#define S1_OFFSET -849491
#define S1_OFFSET -887355
#define S2_SCALE  507.7076064    //Constantes para celda de Carga vertical, c de precalibracion= 505.62
#define S2_OFFSET 384669 //297751 //347000
#define S3_SCALE -1001.3918655264      //Constantes celda de carga horizontal
#define S3_OFFSET 465860 //481281 //480780


void procesaSysEx(void *context, uint8_t command, size_t argc, uint8_t *argv){
  unsigned char canal=argv[0]-1;
  sensores[canal].setFromFirmata(argc,argv);
  sensorsValue[canal]=sensores[canal].getUnits();
  //if(canal==canalFuerza) runPID=true; 
}

void resetCommUART1(){
  Serial1.begin(115200);
}


void printSerial1(float val1,float val2){
  Serial1.print(val1,1);
  Serial1.print(",");
  Serial1.println(val2,0);
}
void printSerial1(float val1,float val2,float val3,float val4){
  Serial1.print(val1,1);
  Serial1.print(",");
  Serial1.print(val2,0);
  Serial1.print(",");
  Serial1.print(val3,1);
  Serial1.print(",");
  Serial1.println(val4,0);
}


void setup() {
  sensores[0].setScale(S1_SCALE);
  sensores[1].setScale(S2_SCALE);
  sensores[2].setScale(S3_SCALE);
  sensores[0].setOffset(S1_OFFSET);
  sensores[1].setOffset(S2_OFFSET);
  sensores[2].setOffset(S3_OFFSET);
  parser.attach(0,procesaSysEx);
  Serial1.begin(115200);

}

void loop() {
  if(Serial1.available()) 
  parser.parse(Serial1.read());
  
  
}
