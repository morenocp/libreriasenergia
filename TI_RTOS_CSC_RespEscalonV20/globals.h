#ifndef GLOBAL_VARIABLES_
#define GLOBAL_VARIABLES_
#include "PMCFirmataConstants.h"
#include "PMCFirmataParser.h"
#include "HX711F.h"
#include "Clock.h"
#include "FPID_v1.h"
#include "ByteBuffer.h"
#include "CFlag.h"
#include "msp432.h"
#include "PinOut.h"
#include "MapPWM.h"
#include "GeneratorCSC.h"
#include "MinMax.h"



//Constants PIDH
#define HDEFAULT_PID_KP 1.397 //1.3 //Incremento un 7.5% debido al cambio en el voltaje de alimentacion 17/1/20
#define HDEFAULT_PID_KI 2.0 //0.85
#define HDEFAULT_PID_KD 0.0
#define HDEFAULT_PID_SAMPLETIME 13
#define HDEFAULT_PID_DELAY_AUTOMATIC 80

//Constants PIDV
#define VDEFAULT_PID_KP 1.075 //1.0 //1.67  //1.3 //Incremento un 7.5% debido al cambio en el voltaje de alimentacion
#define VDEFAULT_PID_KI 0.5 //2.5  //0.85 Nota. En 2.5 el sistema presento inestablidad en una probeta. Ver archivo D6-2810
#define VDEFAULT_PID_KD 0.0
#define VDEFAULT_PID_SAMPLETIME 12
#define VDEFAULT_PID_DELAY_AUTOMATIC 80


//constants MAP2PWM
#define CAL_SLOPE_ASC 1.7 //2.15 //2.0 //1.5511 //0.6447 //Incremento un 7.5% debido al cambio en el voltaje de alimentacion Nota: solamente ase ajustaron los slope, los offset estan pendientes hasta ver el comportamiento
#define CAL_OFFSET_ASC 957   //-617
#define CAL_SLOPE_DES 1.7 //2.15 //2.0  //0.6395
#define CAL_OFFSET_DES 934    //-597
#define CAL_OFFSET_DIF  30   //-30

//constants MAP2PWM
#define CAL_SLOPE_ASC_V 1.8 //2.0  //1.5711   //0.6365
#define CAL_OFFSET_ASC_V 369     //-204
#define CAL_SLOPE_DES_V 1.8 //1.7346 //1.6136   //0.6197
#define CAL_OFFSET_DES_V 254     //-137
#define CAL_OFFSET_DIF_V  50   //-67

//constants index channels
#define canalFuerzaH 2
#define canalLVDTH 4
#define canalPresion 0
#define canalFuerzaV 1
#define canalLVDTV 3
#define canalVolumen 5
#define canalContrapresion 6
#define canalConfinante 7

//Constants of AGC
#define DEFAULT_GAIN_MAX_V 3.0
#define DEFAULT_GAIN_MAX_H 3.0

typedef union {
  float fDato;
  long  lDato;
  unsigned long ulDato;
  unsigned char datos[4];
}  MultiDato;

typedef struct {
  float input;
  float output;
  float sp;
  float kp;
  float ki;
  float kd;
  float preValue;
  uint8_t delayAutomatic;
} PIDData;

//tabla de valores cosenoidales
float const fcoseno[] =
{0.0,
                         0.024471741852423,
                         0.095491502812526,
                         0.206107373853763,
                         0.345491502812526,
                         0.5,
                         0.654508497187474,
                         0.793892626146236,
                         0.904508497187474,
                         0.975528258147577,
                         1.0,
                         0.975528258147577,
                         0.904508497187474,
                         0.793892626146237,
                         0.654508497187474,
                         0.5,
                         0.345491502812526,
                         0.206107373853763,
                         0.095491502812526,
                         0.02447174185242
                        };
float const fseno[]=
{0.0,
0.309016994374947,
0.587785252292473,
0.809016994374947,
0.951056516295153,
1,
0.951056516295153,
0.809016994374947,
0.587785252292473,
0.309016994374947,
0.0,
-0.309016994374948,
-0.587785252292473,
-0.809016994374947,
-0.951056516295153,
-1,
-0.951056516295154,
-0.809016994374947,
-0.587785252292473,
-0.309016994374948
};


//Variable globales

unsigned char firmatabuffer[32];
FirmataParser parser(firmatabuffer,sizeof(firmatabuffer));
unsigned char firmatabufferCommand[32];
FirmataParser parserCommand(firmatabufferCommand,sizeof(firmatabufferCommand));
unsigned char bufferCommand[32];
ByteBuffer miBuffer(bufferCommand,sizeof(bufferCommand));

//Watch dog variables and function

MinMax limitesH(-5000.0,5000.0);
MinMax limitesPWMH(-5000.0,5000.0);
MinMax limitesV(-5000.0,5000.0);
MinMax limitesPWMV(-5000.0,5000.0);
float gainMaxV=DEFAULT_GAIN_MAX_V;
float gainMaxH=DEFAULT_GAIN_MAX_H;

unsigned char wtdi2c=20;
bool sendPWMData=false;

HX711F sensores[]={HX711F(),HX711F(),HX711F(),HX711F(),HX711F(),HX711F(),HX711F(),HX711F()};
float sensorsValue[]={0,0,0,0,0,0,0,0};
Clock clkSampler,clkPID,clkGenerator,clkI2C,clkPIDV;

CFlag runPID,runPIDV,runGenerator,runGeneratorV, sampleI2C;
PinOut monotonico(P2OUT,BIT7,0);

PIDData dpidh={0,0,0,HDEFAULT_PID_KP,HDEFAULT_PID_KI,HDEFAULT_PID_KD,0,0};
PID pidh(&dpidh.input,&dpidh.output,&dpidh.sp,dpidh.kp,dpidh.ki,dpidh.kd,0);
MapPWM mapH(CAL_SLOPE_ASC,CAL_OFFSET_ASC,CAL_SLOPE_DES,CAL_OFFSET_DES);
GeneratorCSC generh(fseno,sizeof(fseno)/4);


PIDData dpidv={0,0,0,VDEFAULT_PID_KP,VDEFAULT_PID_KI,VDEFAULT_PID_KD,0,0};
PID pidv(&dpidv.input,&dpidv.output,&dpidv.sp,dpidv.kp,dpidv.ki,dpidv.kd,0);
MapPWM mapV(CAL_SLOPE_ASC_V,CAL_OFFSET_ASC_V,CAL_SLOPE_DES_V,CAL_OFFSET_DES_V);
GeneratorCSC generv(fcoseno,sizeof(fcoseno)/4);


//functions prototypes
void setOutputPWM(uint16_t value) ;
void setOutputPWMV(uint16_t value) ;
void setDirectionPWM(bool rignt);
void stopPWM();
void intData(void * context, uint8_t command, uint16_t value);
void boolData(void * context, uint8_t command, uint16_t value);
void procesaSysEx(void * context, uint8_t command, size_t argc, uint8_t * argv); 
void procesaSysExCommands(void * context, uint8_t command, size_t argc, uint8_t * argv);                                                                             
MultiDato  decodeFromFirmata(unsigned char argc, unsigned char * argv) ;
void resetCommI2C();
void resetCommUART1();
void clockFunctionSampler();
void clockFunctionPIDV();
void clockFunctionPID();
void clockFunctionI2CSampler();
void clockFunctionGenerator();
void printSerial1(float val1,float val2);
void printSerial1(float val1,float val2,float val3,float val4);
void setDefaultSlopeH();
void setDefaultslopeV();

















#endif
