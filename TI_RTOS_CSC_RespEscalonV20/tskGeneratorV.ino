#include "globals.h"

void updateEqV();

void setup8() {                        
  generv.setIntervalValues(1,10,true);
  dpidv.preValue=0;

}

void loop8() {
 float valSen=0;
 bool asc;
 

  if (runGeneratorV.isSet()&& generv.getEnable()) {
      valSen=generv.getValue(true);
      asc=generv.isSlopeUp();
      dpidv.output=mapV.map(valSen,true);
      setOutputPWMV((uint16_t)dpidv.output);
      updateEqV();
      if (generv.next()){
        dpidv.delayAutomatic=10;//VDEFAULT_PID_DELAY_AUTOMATIC;     
      }
    
  }
}

void setDefaultGainV(float x, float y,float gain){
  float offset=y-gain*x;
  mapV.setSlope(gain,true);
  mapV.setOffset(offset,true);
  mapV.setSlope(gain,false);
  mapV.setOffset(offset,false);
}


void updateEqV(){
   unsigned int phase=generv.getPhase();
   unsigned int cycle=generv.getCycleNumber();
   float minF,maxF,minP,maxP,deltaF;
   limitesV.update(sensorsValue[canalFuerzaV]);
   limitesPWMV.update(dpidv.output);
   
   if(cycle==1 && phase==15) {
    limitesV.reset();
    limitesPWMV.reset();
   }
   
   if(phase==15 && cycle>1){
        minF=limitesV.getMin();
        maxF=limitesV.getMax();
        minP=limitesPWMV.getMin();
        maxP=limitesPWMV.getMax();
        deltaF=limitesV.getDelta();
        if(deltaF==0) setDefaultGainV(maxF,maxP,gainMaxV);
        else
        {
          deltaF=0.5*((limitesPWMV.getDelta()+mapV.getSlope(true)*generv.getAmplitud())/deltaF);
          deltaF=deltaF>gainMaxV?gainMaxV:deltaF;
          setDefaultGainV(maxF,maxP,deltaF);
          
          /*mapV.setPoint2D(0,minF,minP);
          mapV.setPoint2D(1,maxF,maxP);
          mapV.setPoint2D(2,minF,minP);
          mapV.setPoint2D(3,maxF,maxP);
          mapV.updateEqs();
          if (mapV.getSlope(true)>gainMaxV) setDefaultGainV(maxF,maxP,gainMaxV);
           printSerial1(minF,minP,maxF,maxP);*/
        
        }      
        printSerial1(mapV.getSlope(true),mapV.getOffset(true)); 
        limitesV.reset();
        limitesPWMV.reset();
        
   }
       
}



void setDefaultSlopeV(){
  mapV.setSlope(CAL_SLOPE_ASC_V,true);
  mapV.setSlope(CAL_SLOPE_DES_V,false);
  
}
