#include "globals.h"
#include "msp.h"
#include "bitman.h"


#define PWM_PERIODE_V 4095



void clockFunctionPIDV()
{

  runPIDV.set();
  if(dpidv.delayAutomatic && ((--dpidv.delayAutomatic)==0))  pidv.SetMode(AUTOMATIC);
}


void setOutputPWMV(uint16_t value) {
  TIMER_A0->CCR[3] = value;
}



void setup7() {

  
  TIMER_A0->CCR[3] = 0;
  TIMER_A0->CCTL[3] = TIMER_A_CCTLN_OUTMOD_7;

  //Configuracion del PID
  dpidv.sp = 0;                                //Poner valor para probar PID
  pidv.SetMode(MANUAL);
  pidv.SetSampleTime(VDEFAULT_PID_SAMPLETIME);
  pidv.SetTunings(dpidv.kp, dpidv.ki, dpidv.kd);
  pidv.SetOutputLimits(0, (PWM_PERIODE_V-1));
  dpidv.output=0;
  clkPIDV.begin(clockFunctionPIDV, 180, VDEFAULT_PID_SAMPLETIME);
  clkPIDV.start();
}

void loop7() {

 if (runPIDV.isSet()) {
    dpidv.input=sensorsValue[canalFuerzaV];
    pidv.Compute();
    setOutputPWMV((uint16_t) dpidv.output);
  }
 
}
