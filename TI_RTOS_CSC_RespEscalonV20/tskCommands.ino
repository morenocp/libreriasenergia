#include "globals.h"



#define I2C_SLAVE1_ADDRESS 77
#define CMD_SET_PID_SP 1
#define CMD_SET_PID_KP 2
#define CMD_SET_PID_KI 3
#define CMD_SET_PID_KD 4
#define CMD_SET_PID_AUTOMATIC 5
#define CMD_SET_PID_SAMPLETIME 6
#define CMD_SET_PWM_OUT 7
#define CMD_SET_SENSOR1_TARE 8
#define CMD_SET_SENSOR1_SCALE 9
#define CMD_SET_SENSOR1_OFFSET 10


//comando spara el esclavo 2, sensores de presion y desplazamiento (sensor2)
#define I2C_SLAVE2_ADDRESS 87
#define CMD_SET_SENSOR2_TARE 18
#define CMD_SET_SENSOR2_SCALE 19
#define CMD_SET_SENSOR2_OFFSET 20
#define CMD_SET_SENSOR3_TARE 28
#define CMD_SET_SENSOR3_SCALE 29
#define CMD_SET_SENSOR3_OFFSET 30
#define CMD_SET_SENSOR4_TARE 38
#define CMD_SET_SENSOR4_SCALE 39
#define CMD_SET_SENSOR4_OFFSET 40
#define CMD_SET_SENSOR5_TARE 48
#define CMD_SET_SENSOR5_SCALE 49
#define CMD_SET_SENSOR5_OFFSET 50

//Comandos para el generador de funcion
#define CMD_SET_GENER_ENABLE 11
#define CMD_SET_GENER_AMP 12
#define CMD_SET_GENER_OFFSET 13

#define CMD_SET_PID_SP_V 71
#define CMD_SET_PID_KP_V 72
#define CMD_SET_PID_KI_V 73
#define CMD_SET_PID_KD_V 74
#define CMD_SET_PID_AUTOMATIC_V 75
#define CMD_SET_PID_SAMPLETIME_V 76
#define CMD_SET_PWM_OUT_V 14

#define CMD_SET_GENER_ENABLE_V 81
#define CMD_SET_GENER_AMP_V 82
#define CMD_SET_GENER_OFFSET_V 83
#define CMD_SET_MONOTONICO_ENABLE 84
#define CMD_SET_RESET_COMM_I2C 50
#define CMD_SET_RESET_COMM_UART1 51
#define CMD_SET_SEND_PWM_DATA 52
#define CMD_SET_GAIN_MAX_H 53
#define CMD_SET_GAIN_MAX_V 54




void setup5() {
  parserCommand.attach(ANALOG_MESSAGE,intData,0);
  parserCommand.attach(SET_DIGITAL_PIN_VALUE,boolData,0);
   parserCommand.attach(0,procesaSysExCommands);
}



void loop5() {
  if(not(miBuffer.isEmpty()) )  parserCommand.parse(miBuffer.pop());
}






void intData(void * context, uint8_t command, uint16_t value){
 switch (command){
  case CMD_SET_PWM_OUT:
    dpidh.output=value;
    setOutputPWM((long) dpidh.output);
    break;
  case CMD_SET_PWM_OUT_V:
    dpidv.output=value;
    setOutputPWMV((long)dpidv.output);
    break;   
 }
  
}

void boolData(void * context, uint8_t command, uint16_t value){
  float aux;
  switch(command){
    case CMD_SET_SENSOR1_TARE:
    sensores[0].tare();
    break;
    case CMD_SET_SENSOR2_TARE:
    sensores[1].tare();
    break;
    case CMD_SET_SENSOR3_TARE:
    sensores[2].tare();
    break;
    case CMD_SET_SENSOR4_TARE:
    sensores[3].tare();
    break;
    case CMD_SET_SENSOR5_TARE:
    sensores[4].tare();
    break;
    case CMD_SET_GENER_ENABLE:
        if (value && not(generh.getEnable())) {
          setDefaultSlopeH();
          aux=dpidh.output-mapH.delta(generh.getOffset(),true);      
          mapH.setOffset(aux,true);
          mapH.setOffset(aux-CAL_OFFSET_DIF,false);
          generh.setEnable();
          pidh.SetMode(MANUAL);
          limitesH.reset();
        } else if (generh.getEnable() && not(value) ) {
          generh.setStop();
        }
    break;
    case CMD_SET_GENER_ENABLE_V:
        if (value && not(generv.getEnable())) {
          setDefaultSlopeV();
          aux=dpidv.output-mapV.delta(generv.getOffset(),true);       
          mapV.setOffset(aux,true);
          mapV.setOffset(aux-CAL_OFFSET_DIF_V,false);
          generv.setEnable();
          limitesV.reset();
          pidv.SetMode(MANUAL);;
        } else if (generv.getEnable() && not(value) ) {
          generv.setStop();
        }
    break;
    case CMD_SET_PID_AUTOMATIC:  //Establece si el PID esta modo automatico
        
        if (value == 0) {
          pidh.SetMode(MANUAL);
        }
        else pidh.SetMode(AUTOMATIC);
        break;
    case CMD_SET_PID_AUTOMATIC_V:  //Establece si el PID esta modo automatico
        
        if (value == 0) {
          pidv.SetMode(MANUAL);
        }
        else pidv.SetMode(AUTOMATIC);
        break;
    case CMD_SET_MONOTONICO_ENABLE:  //Establece si el PID esta modo automatico
        if (value == 0) monotonico.clear();
        else monotonico.set();
        break;
    case CMD_SET_RESET_COMM_I2C:
        resetCommI2C();
        break;
    case CMD_SET_RESET_COMM_UART1:
        resetCommUART1();
        break;
    case CMD_SET_SEND_PWM_DATA:
        sendPWMData=value;
        break;
  }
}

void procesaSysExCommands(void * context, uint8_t command, size_t argc, uint8_t * argv){
  unsigned char canal=argv[0];
  MultiDato dato=decodeFromFirmata(argc,argv);
  switch (canal) {
      case CMD_SET_PID_SP:     //Establece SetPoint
        dpidh.sp = dato.fDato ;
        break;
      case CMD_SET_PID_KP:     //Establece Kp
        dpidh.kp = dato.fDato ;
        pidh.SetTunings(dpidh.kp, dpidh.ki, dpidh.kd);
        break;
      case CMD_SET_PID_KI:     //Establece Ki
        dpidh.ki = dato.fDato ;
        pidh.SetTunings(dpidh.kp, dpidh.ki, dpidh.kd);
        break;
      case CMD_SET_PID_KD:     //Establece Kd
        dpidh.kd = dato.fDato ;
        pidh.SetTunings(dpidh.kp, dpidh.ki, dpidh.kd);
        break;
      case CMD_SET_PID_SAMPLETIME:     //Establece el periodo de muestreo para el PID
        pidh.SetSampleTime(dato.fDato);
        pidh.SetTunings(dpidh.kp, dpidh.ki, dpidh.kd);
        break;
      case CMD_SET_PID_SP_V:     //Establece SetPoint
        dpidv.sp = dato.fDato ;
        break;
      case CMD_SET_PID_KP_V:     //Establece Kp
        dpidv.kp = dato.fDato ;
        pidv.SetTunings(dpidv.kp, dpidv.ki, dpidv.kd);
        break;
      case CMD_SET_PID_KI_V:     //Establece Ki
        dpidv.ki = dato.fDato ;
        pidv.SetTunings(dpidv.kp, dpidv.ki, dpidv.kd);
        break;
      case CMD_SET_PID_KD_V:     //Establece Kd
        dpidv.kd = dato.fDato ;
        pidv.SetTunings(dpidv.kp, dpidv.ki, dpidv.kd);
        break;
      case CMD_SET_PID_SAMPLETIME_V:     //Establece el periodo de muestreo para el PID
        pidv.SetSampleTime(dato.fDato);
        pidv.SetTunings(dpidv.kp, dpidv.ki, dpidv.kd);
        break;
      case CMD_SET_SENSOR1_SCALE:     //Establece el factor de escala del HX711
        sensores[0].setScale(dato.fDato) ;
        break;
      case CMD_SET_SENSOR2_SCALE:     //Establece el factor de escala del HX711
        sensores[1].setScale(dato.fDato) ;
        break;
      case CMD_SET_SENSOR3_SCALE:     //Establece el factor de escala del HX711
        sensores[2].setScale(dato.fDato) ;
        break;
        case CMD_SET_SENSOR4_SCALE:     //Establece el factor de escala del HX711
        sensores[3].setScale(dato.fDato) ;
        break;
        case CMD_SET_SENSOR5_SCALE:     //Establece el factor de escala del HX711
        sensores[4].setScale(dato.fDato) ;
        break;
        case CMD_SET_GAIN_MAX_H:     //Establece la ganancia maxima del CAG Horizontal
        gainMaxH=dato.fDato ;
        break;
        case CMD_SET_GAIN_MAX_V:     //Establece la ganancia maxima del CAG Vertical
        gainMaxV=dato.fDato ;
        break;
        //Configuracion para generador horizontal
      case CMD_SET_GENER_OFFSET:
        dpidh.sp=dato.fDato;
        pidh.SetMode(MANUAL);
        //dpidh.output=Map2PWM(&mapH,generh.offset,generh.offset >=dpidh.preValue);
        dpidh.output=mapH.map(dpidh.sp,dpidh.sp>=dpidh.preValue);      
        setOutputPWM((uint16_t) dpidh.output);
        dpidh.preValue=dpidh.sp;
        dpidh.delayAutomatic=HDEFAULT_PID_DELAY_AUTOMATIC;
        break;
      case CMD_SET_GENER_AMP:
        generh.setAmplitud(dato.fDato);
        break;
      //Configuracion para generador vertical
      case CMD_SET_GENER_OFFSET_V:
        dpidv.sp=dato.fDato;
        generv.setOffset(dpidv.sp);
        pidv.SetMode(AUTOMATIC);
        dpidv.preValue=dpidv.sp;     
        break;
      case CMD_SET_GENER_AMP_V:
        generv.setAmplitud(dato.fDato);
        break;
     case CMD_SET_SENSOR1_OFFSET:   //Establece el offset del HX711
        sensores[0].setOffset(dato.lDato);
        break;
      case CMD_SET_SENSOR2_OFFSET:   //Establece el offset del HX711
        sensores[1].setOffset(dato.lDato);
        break;
      case CMD_SET_SENSOR3_OFFSET:   //Establece el offset del HX711
        sensores[2].setOffset(dato.lDato);
        break;
        case CMD_SET_SENSOR4_OFFSET:   //Establece el offset del HX711
        sensores[3].setOffset(dato.lDato);
        break;
        case CMD_SET_SENSOR5_OFFSET:   //Establece el offset del HX711
        sensores[4].setOffset(dato.lDato);
        break;
        
  
}
}

MultiDato  decodeFromFirmata(unsigned char argc, unsigned char * argv) {
  MultiDato result;
   int n=argc-1;
   unsigned char i;
   unsigned long value=argv[n];
   for(i=n;i>1;i--) value=(value<<7)|argv[i-1];
   result.lDato=static_cast<long>(value);
   return result;
 }
